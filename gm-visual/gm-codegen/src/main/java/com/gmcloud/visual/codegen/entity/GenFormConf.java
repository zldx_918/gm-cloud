/*
 * Copyright (c) 2020 pig4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmcloud.visual.codegen.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.google.common.base.Objects;
import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 生成记录
 *
 * @since 2019-08-12 15:55:35
 */
@TableName("gen_form_conf")
@Schema(description = "生成记录")
public class GenFormConf extends BaseEntity {

	/**
	 * ID
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@Schema(description = "ID")
	private Long id;

	/**
	 * 表名称
	 */
	@Schema(description = "表名称")
	private String tableName;

	/**
	 * 表单信息
	 */
	@Schema(description = "表单信息")
	private String formInfo;

	/**
	 * 删除标记
	 */
	@Schema(description = "删除标记")
	private String delFlag;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getFormInfo() {
		return formInfo;
	}

	public void setFormInfo(String formInfo) {
		this.formInfo = formInfo;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		GenFormConf that = (GenFormConf) o;
		return Objects.equal(id, that.id) && Objects.equal(tableName, that.tableName) && Objects.equal(formInfo, that.formInfo) && Objects.equal(delFlag, that.delFlag);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id, tableName, formInfo, delFlag);
	}
}
