package com.gmcloud.visual.codegen.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gmcloud.common.utils.R;
import com.gmcloud.common.log.annotation.SysLog;
import com.gmcloud.common.security.annotation.Inner;
import com.gmcloud.visual.codegen.entity.GenDatasourceConf;
import com.gmcloud.visual.codegen.service.GenDatasourceConfService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/23 20:52
 * 数据源管理
 */
@RestController
@RequestMapping("/dsconf")
@Tag(name = "数据源管理模块")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
public class GenDsConfController {

    private final GenDatasourceConfService datasourceConfService;

    public GenDsConfController(GenDatasourceConfService datasourceConfService) {
        this.datasourceConfService = datasourceConfService;
    }

    /**
     * 分页查询
     * @param page 分页对象
     * @param datasourceConf 数据源表
     * @return
     */
    @Inner
    @GetMapping("/page")
    public R<IPage<GenDatasourceConf>> getSysDatasourceConfPage(Page page, GenDatasourceConf datasourceConf) {
        return R.ok(datasourceConfService.page(page, Wrappers.query(datasourceConf)));
    }

    /**
     * 查询全部数据源
     * @return
     */
    @GetMapping("/list")
    public R<List<GenDatasourceConf>> list() {
        return R.ok(datasourceConfService.list());
    }

    /**
     * 通过id查询数据源表
     * @param id id
     * @return Result
     */
    @GetMapping("/{id}")
    public R<GenDatasourceConf> getById(@PathVariable("id") Integer id) {
        return R.ok(datasourceConfService.getById(id));
    }

    /**
     * 新增数据源表
     * @param datasourceConf 数据源表
     * @return Result
     */
    @SysLog("新增数据源表")
    @PostMapping
    public R<Boolean> save(@RequestBody GenDatasourceConf datasourceConf) {
        return R.ok(datasourceConfService.saveDsByEnc(datasourceConf));
    }

    /**
     * 修改数据源表
     * @param conf 数据源表
     * @return Result
     */
    @SysLog("修改数据源表")
    @PutMapping
    public R<Boolean> updateById(@RequestBody GenDatasourceConf conf) {
        return R.ok(datasourceConfService.updateDsByEnc(conf));
    }

    /**
     * 通过id删除数据源表
     * @param id id
     * @return Result
     */
    @SysLog("删除数据源表")
    @DeleteMapping("/{id}")
    public R<Boolean> removeById(@PathVariable Long id) {
        return R.ok(datasourceConfService.removeByDsId(id));
    }

}
