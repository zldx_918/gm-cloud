package com.gmcloud.visual.codegen;

import com.gmcloud.common.datasource.annotation.EnableDynamicDataSource;
import com.gmcloud.common.feign.annotation.EnableGmFeignClients;
import com.gmcloud.common.security.annotation.EnableGmResourceServer;
import com.gmcloud.common.swagger.annotation.EnableGmDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/23 20:36
 */
@EnableGmDoc
@EnableGmFeignClients
@EnableGmResourceServer
@EnableDynamicDataSource
@EnableDiscoveryClient
@SpringBootApplication
public class GmCodeGenApplication {
    public static void main(String[] args) {
        SpringApplication.run(GmCodeGenApplication.class, args);
    }

}
