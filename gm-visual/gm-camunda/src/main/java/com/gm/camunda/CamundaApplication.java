package com.gm.camunda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zl
 * @since  2022/9/23 17:12
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gm.following
 * @description 工作流启动类
 */
@SpringBootApplication
public class CamundaApplication {

    public static void main(String[] args) {
        SpringApplication.run(CamundaApplication.class, args);
    }
}
