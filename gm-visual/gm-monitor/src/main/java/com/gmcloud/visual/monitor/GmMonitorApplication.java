package com.gmcloud.visual.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/28 15:43
 * 监控中心
 */
@EnableAdminServer
@EnableDiscoveryClient
@SpringBootApplication
public class GmMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmMonitorApplication.class);
    }
}
