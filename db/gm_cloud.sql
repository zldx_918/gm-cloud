/*
 Navicat Premium Data Transfer

 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Schema         : gm_cloud

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 24/03/2023 10:36:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_api`;
CREATE TABLE `sys_api`  (
  `api_id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `api_name` varchar(255)   NULL DEFAULT NULL COMMENT 'api名称',
  `path` varchar(255)   NULL DEFAULT NULL COMMENT 'api地址',
  `method` varchar(255)   NULL DEFAULT 'GET' COMMENT '请求方法,GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS, TRACE;\n必须是这几样',
  `request_data_type` varchar(255)   NULL DEFAULT NULL COMMENT '请求数据类型',
  `response_data_type` varchar(255)   NULL DEFAULT NULL COMMENT '响应数据类型',
  `request_parameters` json NULL COMMENT '请求参数',
  `response_status` json NULL COMMENT '响应状态',
  `response_parameters` json NULL COMMENT '响应参数',
  `response_example` json NULL COMMENT '响应示例',
  `is_ignore` bit(1) NULL DEFAULT b'0' COMMENT '0,需要验证,1忽略验证',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新着',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `del_flag` int NULL DEFAULT 0 COMMENT '0 未删除 1已删除',
  PRIMARY KEY (`api_id`) USING BTREE
) ENGINE = InnoDB    ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_api
-- ----------------------------
INSERT INTO `sys_api` VALUES (1, '获取验证码', '/api/system/auth/code', 'GET', 'application/x-www-form-urlencoded', NULL, NULL, NULL, NULL, NULL, b'1', 'admin', NULL, NULL, '2021-12-28 17:22:20', 0);
INSERT INTO `sys_api` VALUES (2, '登录', '/api/system/auth/login', 'POST', 'application/x-www-form-urlencoded', '.*.', NULL, NULL, NULL, NULL, b'1', 'admin', NULL, NULL, '2021-12-30 11:40:49', 0);
INSERT INTO `sys_api` VALUES (3, '退出', '/api/system/auth/logout', 'DELETE', NULL, '.*.', NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2021-12-31 10:36:24', 0);
INSERT INTO `sys_api` VALUES (4, '获取当前登录用户详情', '/api/system/user/info', 'GET', NULL, '.*.', NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2021-12-31 10:38:36', 0);
INSERT INTO `sys_api` VALUES (5, '构建菜单', '/api/system/menu/nav/build', 'GET', NULL, '.*.', NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2021-12-31 14:06:58', 0);
INSERT INTO `sys_api` VALUES (6, 'api文档', '/doc.html', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'1', 'admin', NULL, NULL, '2022-03-20 14:28:39', 0);
INSERT INTO `sys_api` VALUES (7, '分页查询用户列表', '/api/system/user/page', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-04-25 23:34:16', 0);
INSERT INTO `sys_api` VALUES (8, '批量获取字典', '/api/system/dict/dictNames', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-09 14:20:21', 0);
INSERT INTO `sys_api` VALUES (9, '获取所有的角色', '/api/system/role', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-11 21:26:16', 0);
INSERT INTO `sys_api` VALUES (10, '获取组织树', '/api/unit/organization/getTree', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-14 16:27:00', 0);
INSERT INTO `sys_api` VALUES (11, '获取职位', '/api/unit/position', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-17 14:43:45', 0);
INSERT INTO `sys_api` VALUES (12, '新增用户', '/api/system/user', 'POST', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-17 06:43:37', 0);
INSERT INTO `sys_api` VALUES (13, '删除用户', '/api/system/user', 'DELETE', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-19 14:01:10', 0);
INSERT INTO `sys_api` VALUES (14, '分页查询角色列表', '/api/system/role/page', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-24 13:42:21', 0);
INSERT INTO `sys_api` VALUES (15, '菜单树', '/api/system/menu/tree', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-25 09:02:37', 0);
INSERT INTO `sys_api` VALUES (16, '更新角色关联菜单', '/api/system/role/updateRoleAndMenus', 'PUT', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-26 03:31:30', 0);
INSERT INTO `sys_api` VALUES (17, '获取所有api', '/api/system/api', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-26 04:48:43', 0);
INSERT INTO `sys_api` VALUES (18, '更新角色管理api', '/api/system/role/updateRoleAndApis', 'PUT', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-26 13:51:21', 0);
INSERT INTO `sys_api` VALUES (19, '分页查询菜单列表', '/api/system/menu/page', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-29 15:14:50', 0);
INSERT INTO `sys_api` VALUES (20, '根据pid查询当前子菜单', '/api/system/menu/[0-9]', 'GET', NULL, NULL, NULL, NULL, NULL, NULL, b'0', 'admin', NULL, NULL, '2022-06-30 13:08:02', 0);

-- ----------------------------
-- Table structure for sys_api_dir
-- ----------------------------
DROP TABLE IF EXISTS `sys_api_dir`;
CREATE TABLE `sys_api_dir`  (
  `api_dir_id` int NOT NULL AUTO_INCREMENT,
  `dir_name` varchar(255)   NOT NULL,
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新着',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`api_dir_id`) USING BTREE
) ENGINE = InnoDB   COMMENT = 'api目录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_api_dir
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` int NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级部门',
  `sub_count` int NULL DEFAULT 0 COMMENT '子部门数目',
  `name` varchar(255)   NOT NULL COMMENT '名称',
  `dept_sort` int NULL DEFAULT 999 COMMENT '排序',
  `enabled` bit(1) NOT NULL COMMENT '状态',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB   COMMENT = '部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255)   NOT NULL COMMENT '字典名称',
  `description` varchar(255)   NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `enabled` bit(1) NOT NULL COMMENT '状态',
  `flag` int NULL DEFAULT 0,
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `sys_dict_name_uindex`(`name` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '数据字典' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'user_status', '用户状态', NULL, NULL, '2019-10-27 20:31:36', NULL, b'1', 0);
INSERT INTO `sys_dict` VALUES (4, 'dept_status', '部门状态', NULL, NULL, '2019-10-27 20:31:36', NULL, b'1', 0);
INSERT INTO `sys_dict` VALUES (5, 'job_status', '岗位状态', NULL, 'admin', '2019-10-27 20:31:36', '2021-09-14 13:41:49', b'1', 0);

-- ----------------------------
-- Table structure for sys_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail`  (
  `detail_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_id` bigint NULL DEFAULT NULL COMMENT '字典id',
  `label` varchar(255)   NOT NULL COMMENT '字典标签',
  `value` varchar(255)   NOT NULL COMMENT '字典值',
  `dict_sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `flag` int NULL DEFAULT 0,
  PRIMARY KEY (`detail_id`) USING BTREE,
  INDEX `FK5tpkputc6d9nboxojdbgnpmyb`(`dict_id` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '数据字典详情' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES (1, 1, '启用', '1', 1, NULL, NULL, '2021-04-12 20:31:36', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (2, 1, '禁用', '0', 2, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (3, 4, '启用', '1', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (4, 4, '停用', '0', 2, NULL, NULL, '2021-04-12 20:31:36', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (27, 5, '启用', '1', 1, 'admin', NULL, '2021-09-14 13:41:50', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (28, 5, '停用', '0', 2, 'admin', NULL, '2021-09-14 13:41:50', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (29, 1, '锁定', '2', 3, 'admin', NULL, '2022-06-11 15:49:38', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (30, 1, '密码过期', '3', 4, 'admin', NULL, '2022-06-11 15:50:04', NULL, 0);
INSERT INTO `sys_dict_detail` VALUES (31, 1, '注销', '4', 5, 'admin', NULL, '2022-06-11 15:50:24', NULL, 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级菜单ID,为空就是布局，0,目录,1,路由,2 按钮',
  `sub_count` int NULL DEFAULT 0 COMMENT '子菜单数目',
  `type` tinyint NULL DEFAULT NULL COMMENT '菜单类型',
  `title` varchar(255)   NULL DEFAULT NULL COMMENT '菜单标题',
  `name` varchar(255)   NULL DEFAULT NULL COMMENT '组件名称',
  `component` varchar(255)   NULL DEFAULT NULL COMMENT '组件',
  `menu_sort` int NULL DEFAULT NULL COMMENT '排序',
  `icon` varchar(255)   NULL DEFAULT NULL COMMENT '图标',
  `path` varchar(255)   NULL DEFAULT NULL COMMENT '链接地址',
  `frame` bit(1) NULL DEFAULT b'0' COMMENT '是否外链',
  `cache` bit(1) NULL DEFAULT b'0' COMMENT '缓存',
  `hidden` bit(1) NULL DEFAULT b'0' COMMENT '隐藏',
  `permission` varchar(255)   NULL DEFAULT NULL COMMENT '权限',
  `requires_auth` bit(1) NOT NULL DEFAULT b'1' COMMENT '是否需要验证',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int NULL DEFAULT 0 COMMENT '0 未删除 1已删除',
  PRIMARY KEY (`menu_id`) USING BTREE,
  UNIQUE INDEX `uniq_title`(`title` ASC) USING BTREE,
  UNIQUE INDEX `uniq_name`(`name` ASC) USING BTREE,
  INDEX `inx_pid`(`pid` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '系统菜单' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, NULL, 3, 0, '系统管理', '', '', 1, 'system', 'system', b'0', b'0', b'0', '', b'1', 'admin', NULL, '2018-12-18 15:11:29', '2021-07-29 10:07:13', 0);
INSERT INTO `sys_menu` VALUES (2, 1, 3, 1, '用户管理', 'User', 'system/user/index', 2, 'peoples', 'user', b'0', b'0', b'0', 'user:list', b'1', 'admin', NULL, '2018-12-18 15:14:44', '2021-07-29 15:26:55', 0);
INSERT INTO `sys_menu` VALUES (3, 1, 0, 1, '角色管理', 'Role', 'system/role/index', 3, 'role', 'role', b'0', b'0', b'0', 'roles:list', b'1', 'admin', NULL, '2018-12-18 15:16:07', '2021-07-24 15:03:34', 0);
INSERT INTO `sys_menu` VALUES (4, 1, 0, 1, '菜单管理', 'Menu', 'system/menu/index', 5, 'menu', 'menu', b'0', b'0', b'0', 'menu:list', b'1', 'admin', NULL, '2018-12-18 15:17:28', '2021-07-29 14:59:52', 0);
INSERT INTO `sys_menu` VALUES (5, 2, 0, 2, '新增', 'userAdd', '', 6, '', '/api/user', b'0', b'0', b'0', 'user:add', b'1', 'admin', NULL, '2021-07-29 10:35:09', '2021-07-29 10:35:39', 0);
INSERT INTO `sys_menu` VALUES (6, 2, 0, 2, '删除', 'userDel', '', 7, '', '/api/user', b'0', b'0', b'0', 'user:del', b'1', 'admin', NULL, '2021-07-29 15:10:45', NULL, 0);
INSERT INTO `sys_menu` VALUES (7, 2, 0, 2, '编辑', 'userEdit', '', 8, '', '/api/edit', b'0', b'0', b'0', 'user:deit', b'1', 'admin', NULL, '2021-07-29 15:26:55', NULL, 0);
INSERT INTO `sys_menu` VALUES (8, 1, 0, 1, '用户详情', 'UserDetail', 'system/user/UserDetail', 6, '', 'userDetail/:id', b'0', b'0', b'1', 'user:detail', b'1', 'admin', NULL, '2021-07-29 10:35:09', '2021-07-29 10:35:39', 0);

-- ----------------------------
-- Table structure for sys_oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `sys_oauth_client_details`;
CREATE TABLE `sys_oauth_client_details`  (
  `client_id` varchar(32)   NOT NULL COMMENT '客户端ID',
  `resource_ids` varchar(256)   NULL DEFAULT NULL COMMENT '资源列表',
  `client_secret` varchar(256)   NULL DEFAULT NULL COMMENT '客户端密钥',
  `scope` varchar(256)   NULL DEFAULT NULL COMMENT '域',
  `authorized_grant_types` varchar(256)   NULL DEFAULT NULL COMMENT '认证类型',
  `web_server_redirect_uri` varchar(256)   NULL DEFAULT NULL COMMENT '重定向地址',
  `authorities` varchar(256)   NULL DEFAULT NULL COMMENT '角色列表',
  `access_token_validity` int NULL DEFAULT NULL COMMENT 'token 有效期',
  `refresh_token_validity` int NULL DEFAULT NULL COMMENT '刷新令牌有效期',
  `additional_information` varchar(4096)   NULL DEFAULT NULL COMMENT '令牌扩展字段JSON',
  `autoapprove` varchar(256)   NULL DEFAULT NULL COMMENT '是否自动放行',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64)   NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64)   NULL DEFAULT NULL COMMENT '更新人',
  PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB   COMMENT = '终端信息表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_oauth_client_details
-- ----------------------------
INSERT INTO `sys_oauth_client_details` VALUES ('app', NULL, 'app', 'server', 'app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('client', NULL, 'client', 'server', 'client_credentials', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('daemon', NULL, 'daemon', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('gen', NULL, 'gen', 'server', 'password,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('gm', NULL, 'gm', 'server', 'password,app,refresh_token,authorization_code,client_credentials', 'https://baidu.com', NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);
INSERT INTO `sys_oauth_client_details` VALUES ('test', NULL, 'test', 'server', 'password,app,refresh_token', NULL, NULL, NULL, NULL, NULL, 'true', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `post_code` varchar(30)   NULL DEFAULT NULL COMMENT '岗位编码',
  `post_name` varchar(255)   NOT NULL COMMENT '岗位名称',
  `enabled` bit(1) NOT NULL COMMENT '岗位状态',
  `post_sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255)   NULL DEFAULT NULL COMMENT '备注',
  `del_flag` int NULL DEFAULT 0 COMMENT '0未删除 1已删除',
  PRIMARY KEY (`post_id`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`post_name` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '岗位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_name` varchar(255)   NOT NULL COMMENT '名称',
  `role_code` varchar(20)   NULL DEFAULT NULL COMMENT '角色代码',
  `level` int NULL DEFAULT NULL COMMENT '角色级别',
  `description` varchar(255)   NULL DEFAULT NULL COMMENT '描述',
  `data_scope` varchar(255)   NULL DEFAULT NULL COMMENT '数据权限',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '1激活，0禁用',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int NOT NULL DEFAULT 0 COMMENT '0 未删除 1已删除',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`role_name` ASC) USING BTREE,
  INDEX `role_name_index`(`role_name` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'ROLE_超级管理员', NULL, 0, '-', '全部', b'1', 'admin', 'admin', '2021-02-03 11:04:37', '2021-09-13 09:25:01', 0);
INSERT INTO `sys_role` VALUES (2, 'ROLE_普通用户', NULL, 1, '普通用户', '本级', b'1', 'admin', NULL, '2021-04-14 15:40:09', '2021-09-03 15:48:06', 0);

-- ----------------------------
-- Table structure for sys_role_api
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_api`;
CREATE TABLE `sys_role_api`  (
  `role_id` int NOT NULL COMMENT '角色id',
  `api_id` int NOT NULL COMMENT 'API ID'
) ENGINE = InnoDB   COMMENT = '角色对应api' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_api
-- ----------------------------
INSERT INTO `sys_role_api` VALUES (1, 5);
INSERT INTO `sys_role_api` VALUES (1, 4);
INSERT INTO `sys_role_api` VALUES (1, 3);
INSERT INTO `sys_role_api` VALUES (1, 2);
INSERT INTO `sys_role_api` VALUES (1, 1);
INSERT INTO `sys_role_api` VALUES (1, 7);
INSERT INTO `sys_role_api` VALUES (1, 6);
INSERT INTO `sys_role_api` VALUES (1, 8);
INSERT INTO `sys_role_api` VALUES (1, 9);
INSERT INTO `sys_role_api` VALUES (1, 11);
INSERT INTO `sys_role_api` VALUES (1, 10);
INSERT INTO `sys_role_api` VALUES (1, 12);
INSERT INTO `sys_role_api` VALUES (1, 13);
INSERT INTO `sys_role_api` VALUES (1, 14);
INSERT INTO `sys_role_api` VALUES (1, 15);
INSERT INTO `sys_role_api` VALUES (1, 16);
INSERT INTO `sys_role_api` VALUES (1, 17);
INSERT INTO `sys_role_api` VALUES (1, 18);
INSERT INTO `sys_role_api` VALUES (2, 4);
INSERT INTO `sys_role_api` VALUES (2, 3);
INSERT INTO `sys_role_api` VALUES (2, 5);
INSERT INTO `sys_role_api` VALUES (2, 7);
INSERT INTO `sys_role_api` VALUES (1, 19);
INSERT INTO `sys_role_api` VALUES (1, 20);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`menu_id`, `role_id`) USING BTREE,
  INDEX `FKcngg2qadojhi3a651a5adkvbq`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB   COMMENT = '角色菜单关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (3, 1);
INSERT INTO `sys_role_menu` VALUES (4, 1);
INSERT INTO `sys_role_menu` VALUES (5, 1);
INSERT INTO `sys_role_menu` VALUES (6, 1);
INSERT INTO `sys_role_menu` VALUES (7, 1);
INSERT INTO `sys_role_menu` VALUES (8, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (3, 2);
INSERT INTO `sys_role_menu` VALUES (4, 2);
INSERT INTO `sys_role_menu` VALUES (5, 2);
INSERT INTO `sys_role_menu` VALUES (6, 2);
INSERT INTO `sys_role_menu` VALUES (7, 2);

-- ----------------------------
-- Table structure for sys_unit
-- ----------------------------
DROP TABLE IF EXISTS `sys_unit`;
CREATE TABLE `sys_unit`  (
  `unit_id` int NOT NULL COMMENT '单位id',
  `unit_name` varchar(255)   NULL DEFAULT NULL COMMENT '单位名称',
  `unit_addrees` varchar(255)   NULL DEFAULT NULL COMMENT '单位地址',
  `unit_tel` varchar(255)   NULL DEFAULT NULL COMMENT '单位电话',
  PRIMARY KEY (`unit_id`) USING BTREE
) ENGINE = InnoDB   ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_unit
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(255)   NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255)   NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(255)   NULL DEFAULT NULL COMMENT '随机盐',
  `nick_name` varchar(255)   NULL DEFAULT NULL COMMENT '昵称',
  `gender` varchar(2)   NULL DEFAULT NULL COMMENT '性别',
  `phone` varchar(255)   NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(255)   NULL DEFAULT NULL COMMENT '邮箱',
  `avatar` varchar(255)   NULL DEFAULT NULL COMMENT '头像',
  `birthday` datetime NULL DEFAULT NULL,
  `admin` bit(1) NULL DEFAULT b'0' COMMENT '是否为admin账号',
  `status` varchar(2)   NULL DEFAULT '1' COMMENT '状态：1启用、0禁用,2锁定，3密码过期，4，已注销',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建者',
  `pwd_reset_time` datetime NULL DEFAULT NULL COMMENT '修改密码的时间',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新着',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` int NULL DEFAULT 0 COMMENT '0 未删除 1已删除',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `UK_kpubos9gc2cvtkb0thktkbkes`(`email` ASC) USING BTREE,
  UNIQUE INDEX `username`(`username` ASC) USING BTREE,
  UNIQUE INDEX `uniq_username`(`username` ASC) USING BTREE,
  UNIQUE INDEX `uniq_email`(`email` ASC) USING BTREE,
  INDEX `FKpq2dhypk2qgt68nauh2by22jb`(`avatar` ASC) USING BTREE,
  INDEX `inx_enabled`(`status` ASC) USING BTREE
) ENGINE = InnoDB    COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC', NULL, '管理员', '男', '16608054836', '290187924@qq.com', 'avatar', '1995-02-13 01:14:21', b'1', '0', 'admin', '2021-07-31 14:37:04', 'admin', '2021-02-03 09:11:56', '2022-06-18 22:41:21', 0);
INSERT INTO `sys_user` VALUES (2, '吕婷', '$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC', NULL, '小吕', '女', '13096299666', '1791195756@qq.com', NULL, '2021-07-30 10:40:19', b'0', '0', 'admin', '2021-08-02 20:58:46', 'admin', '2021-04-19 10:16:28', '2022-07-14 15:31:36', 0);
INSERT INTO `sys_user` VALUES (3, '何天佑', '$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC', NULL, '秀秀', '男', '123', '2312312', NULL, '2022-06-19 12:19:09', b'0', '0', 'admin', NULL, 'admin', '2022-06-19 00:19:27', '2022-07-14 15:31:40', 0);

-- ----------------------------
-- Table structure for sys_user_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_dept`;
CREATE TABLE `sys_user_dept`  (
  `user_id` int NOT NULL COMMENT '用户id',
  `dept_id` int NOT NULL COMMENT '部门id'
) ENGINE = InnoDB   COMMENT = '用户部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_organization
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_organization`;
CREATE TABLE `sys_user_organization`  (
  `user_id` int NOT NULL COMMENT '用户id',
  `organization_id` int NOT NULL COMMENT '组织id'
) ENGINE = InnoDB   COMMENT = '用户部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_organization
-- ----------------------------
INSERT INTO `sys_user_organization` VALUES (1, 8);
INSERT INTO `sys_user_organization` VALUES (1, 7);
INSERT INTO `sys_user_organization` VALUES (5, 8);
INSERT INTO `sys_user_organization` VALUES (5, 7);
INSERT INTO `sys_user_organization` VALUES (24, 7);
INSERT INTO `sys_user_organization` VALUES (21, 6);
INSERT INTO `sys_user_organization` VALUES (25, 8);

-- ----------------------------
-- Table structure for sys_user_position
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_position`;
CREATE TABLE `sys_user_position`  (
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `position_id` int NULL DEFAULT NULL COMMENT '职位id'
) ENGINE = InnoDB   ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_position
-- ----------------------------
INSERT INTO `sys_user_position` VALUES (1, 1);
INSERT INTO `sys_user_position` VALUES (5, 1);
INSERT INTO `sys_user_position` VALUES (24, 1);
INSERT INTO `sys_user_position` VALUES (21, 1);
INSERT INTO `sys_user_position` VALUES (20, 1);
INSERT INTO `sys_user_position` VALUES (25, 1);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `post_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB   ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (1, 2);
INSERT INTO `sys_user_post` VALUES (5, 2);
INSERT INTO `sys_user_post` VALUES (20, 3);
INSERT INTO `sys_user_post` VALUES (21, 4);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `FKq4eq273l04bpu4efj0jd0jb98`(`role_id` ASC) USING BTREE
) ENGINE = InnoDB   COMMENT = '用户角色关联' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (5, 1);
INSERT INTO `sys_user_role` VALUES (1, 2);
INSERT INTO `sys_user_role` VALUES (2, 2);
INSERT INTO `sys_user_role` VALUES (3, 2);
INSERT INTO `sys_user_role` VALUES (5, 2);
INSERT INTO `sys_user_role` VALUES (20, 2);
INSERT INTO `sys_user_role` VALUES (21, 2);
INSERT INTO `sys_user_role` VALUES (24, 2);
INSERT INTO `sys_user_role` VALUES (25, 2);

-- ----------------------------
-- Table structure for unit_organization
-- ----------------------------
DROP TABLE IF EXISTS `unit_organization`;
CREATE TABLE `unit_organization`  (
  `organization_id` int NOT NULL AUTO_INCREMENT COMMENT '单位id',
  `organization_name` varchar(255)   NULL DEFAULT NULL COMMENT '组织名称',
  `organization_address` varchar(255)   NULL DEFAULT NULL COMMENT '组织地址',
  `organization_tel` varchar(255)   NULL DEFAULT NULL COMMENT '组织电话',
  `pid` int NULL DEFAULT NULL COMMENT '上级节点',
  `sub_count` int NOT NULL DEFAULT 0 COMMENT '下级节点数',
  `type` varchar(2)   NULL DEFAULT '2' COMMENT '1,组织  2，部门',
  `organization_sort` int NULL DEFAULT 999 COMMENT '排序,数字越小越排前',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '组织状态,1启用，0禁用',
  `create_by` varchar(50)   NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(50)   NULL DEFAULT NULL COMMENT '更新人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新日期',
  `flag` int NULL DEFAULT 0 COMMENT '标记,逻辑已删除值(默认为 1),逻辑未删除值(默认为 0)',
  PRIMARY KEY (`organization_id`) USING BTREE
) ENGINE = InnoDB    COMMENT = '单位组织' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit_organization
-- ----------------------------
INSERT INTO `unit_organization` VALUES (1, '总部', '全国', '110', NULL, 4, '1', 1, b'1', NULL, NULL, '2022-06-14 08:29:34', NULL, 0);
INSERT INTO `unit_organization` VALUES (2, '四川分部', '四川', '112', 1, 0, '1', 2, b'1', NULL, NULL, '2022-06-14 08:32:01', NULL, 0);
INSERT INTO `unit_organization` VALUES (3, '北京分部', '北京', '112', 1, 0, '1', 1, b'1', NULL, NULL, '2022-06-14 08:32:53', NULL, 0);
INSERT INTO `unit_organization` VALUES (4, '上海分部', '上海', '113', 1, 0, '1', 3, b'1', NULL, NULL, '2022-06-14 08:33:15', NULL, 0);
INSERT INTO `unit_organization` VALUES (5, '外部单位', NULL, '11', NULL, 2, '1', 999, b'1', NULL, NULL, '2022-06-14 08:34:19', NULL, 0);
INSERT INTO `unit_organization` VALUES (6, '峨眉', '峨眉', '121', 5, 0, '0', 2, b'1', NULL, NULL, '2022-06-14 08:34:36', NULL, 0);
INSERT INTO `unit_organization` VALUES (7, '巴中', '巴中', '12321', 5, 0, '0 ', 1, b'1', NULL, NULL, '2022-06-14 08:34:48', NULL, 0);
INSERT INTO `unit_organization` VALUES (8, '总经办', NULL, NULL, 1, 0, '2', 999, b'1', NULL, NULL, '2022-06-15 13:40:43', NULL, 0);

-- ----------------------------
-- Table structure for unit_position
-- ----------------------------
DROP TABLE IF EXISTS `unit_position`;
CREATE TABLE `unit_position`  (
  `position_id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `position_name` varchar(255)   NULL DEFAULT NULL COMMENT '名称',
  `organization_id` int NULL DEFAULT NULL COMMENT '所属组织id',
  `enabled` bit(1) NULL DEFAULT b'1' COMMENT '是否启用 ,1启用,0禁用',
  `position_sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255)   NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(255)   NULL DEFAULT NULL COMMENT '更新人',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`position_id`) USING BTREE
) ENGINE = InnoDB    COMMENT = '职位' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of unit_position
-- ----------------------------
INSERT INTO `unit_position` VALUES (1, 'java开发工程师', 1, b'1', 1, NULL, NULL, '2022-06-16 00:09:23', NULL);

-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
                         `id` bigint NOT NULL,
                         `type` char(1) COLLATE utf8mb4_bin DEFAULT '1' COMMENT '日志类型',
                         `title` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '日志标题',
                         `service_id` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '服务ID',
                         `remote_addr` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作IP地址',
                         `user_agent` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户代理',
                         `request_uri` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '请求URI',
                         `method` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作方式',
                         `params` text COLLATE utf8mb4_bin COMMENT '操作提交的数据',
                         `time` bigint DEFAULT NULL COMMENT '执行时间',
                         `del_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '删除标记',
                         `exception` text COLLATE utf8mb4_bin COMMENT '异常信息',
                         `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                         `update_time` datetime DEFAULT NULL COMMENT '更新时间',
                         `create_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
                         `update_by` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
                         PRIMARY KEY (`id`),
                         KEY `sys_log_create_by` (`create_by`),
                         KEY `sys_log_request_uri` (`request_uri`),
                         KEY `sys_log_type` (`type`),
                         KEY `sys_log_create_date` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='日志表';

SET FOREIGN_KEY_CHECKS = 1;
