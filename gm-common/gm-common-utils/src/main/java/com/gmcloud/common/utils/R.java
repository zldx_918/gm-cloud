package com.gmcloud.common.utils;


import com.gmcloud.common.utils.constant.CommonConstants;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 22:51
 */
public class R<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 5937204312094649174L;
    private int code;

    private String message;

    private transient  T result;

    public static <T> R<T> ok() {
        return restResult(null, CommonConstants.SUCCESS, "OK");
    }

    public static <T> R<T> ok(T data) {
        return restResult(data, CommonConstants.SUCCESS, "OK");
    }

    public static <T> R<T> ok(T data, String msg) {
        return restResult(data, CommonConstants.SUCCESS, msg);
    }

    public static <T> R<T> failed() {
        return restResult(null, CommonConstants.FAIL, "fail");
    }

    public static <T> R<T> failed(String msg) {
        return restResult(null, CommonConstants.FAIL, msg);
    }

    public static <T> R<T> failed(T data) {
        return restResult(data, CommonConstants.FAIL, "fail");
    }

    public static <T> R<T> failed(T data, String msg) {
        return restResult(data, CommonConstants.FAIL, msg);
    }

    public static <T> R<T> restResult(T result, int code, String message) {
        R<T> apiR = new R<>();
        apiR.setCode(code);
        apiR.setResult(result);
        apiR.setMessage(message);
        return apiR;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "R{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", result=" + result +
                '}';
    }
}
