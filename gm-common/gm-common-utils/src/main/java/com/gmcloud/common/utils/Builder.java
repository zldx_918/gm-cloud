package com.gmcloud.common.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2021/2/9 9:48
 * 类实例化
 */


public class Builder<T> {
    private final Supplier<T> instantiate;

    private final List<Consumer<T>> instanceModifiers = new ArrayList<>();

    public Builder(Supplier<T> instantiate) {
        this.instantiate = instantiate;
    }

    public static <T> Builder<T> of(Supplier<T> instantiate) {
        return new Builder<>(instantiate);
    }

    public <U> Builder<T> with(BiConsumer<T, U> consumer, U value) {
        Consumer<T> c = instance -> consumer.accept(instance, value);
        instanceModifiers.add(c);
        return this;
    }

    public T build() {
        T value = instantiate.get();
        instanceModifiers.forEach(modifier -> modifier.accept(value));
        instanceModifiers.clear();
        return value;
    }
}