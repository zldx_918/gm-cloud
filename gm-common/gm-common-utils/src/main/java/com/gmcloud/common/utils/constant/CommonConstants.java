package com.gmcloud.common.utils.constant;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 21:49
 */
public final class CommonConstants {

    private CommonConstants() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 菜单树根节点
     */
    public static final Long MENU_TREE_ROOT_ID = null;

    /**
     * 菜单
     */
    public static final Long MENU = 0L;

    /**
     * 编码
     */
    public static final String UTF8 = "UTF-8";

    /**
     * JSON 资源
     */
    public static final String CONTENT_TYPE = "application/json; charset=utf-8";

    /**
     * 前端工程名
     */
    public static final String FRONT_END_PROJECT = "gm-ui";

    /**
     * 后端工程名
     */
    public static final String BACK_END_PROJECT = "gm";

    /**
     * 成功标记
     */
    public static final Integer SUCCESS = 200;

    /**
     * 失败标记
     */
    public static final Integer FAIL = 500;

    /**
     * 验证码前缀
     */
    public static final String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY_";

    /**
     * 当前页
     */
    public static final String CURRENT = "current";

    /**
     * size
     */
    public static final String SIZE = "size";
}
