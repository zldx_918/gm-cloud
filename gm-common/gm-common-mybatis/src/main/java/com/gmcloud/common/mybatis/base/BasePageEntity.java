package com.gmcloud.common.mybatis.base;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 13:25
 * @description 基本分页
 */
@Schema(title = "BasePageEntity",description = "通用分页")
public class BasePageEntity<T>  {

    @Schema(description = "当前分页数据")
    private List<T> items;

    @Schema(description = "总数")
    private Long total;

    public BasePageEntity(List<T> items, long total) {
        this.items = items;
        this.total = total;
    }

    public BasePageEntity() {
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}