package com.gmcloud.common.mybatis.annotation;

import com.gmcloud.common.mybatis.base.BaseQueryType;
import org.springframework.web.bind.annotation.ValueConstants;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 13:45
 * @description 通用查询注解
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface QueryAnnotation {

    /**
     * 字段名称
     */
    String fieldName() default "";

    /**
     * 查询方式
     */
    BaseQueryType type() default BaseQueryType.EQUAL;

    /**
     * 忽略不需要拼接的字段
     */
    boolean ignore() default false;

    /**
     * 默认值
     *
     * @return 默认值
     */
    String defaultValue() default ValueConstants.DEFAULT_NONE;

    boolean isAsc() default false;
}
