package com.gmcloud.common.mybatis.base;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 13:51
 * @description 基本查询方式
 */
public enum BaseQueryType {
    //等于
    EQUAL
    //大于等于
    , GREATER_THAN
    //小于等于
    , LESS_THAN
    //模糊查询
    , LIKE
    //左模糊查询
    , LEFT_LIKE
    //右模糊查询
    , RIGHT_LIKE
    //小于
    , LESS_THAN_NQ
    //大于
    , GREATER_THAN_NQ
    //包含
    , IN
    // 不等于
    , NOT_EQUAL
    // between
    , BETWEEN
    // 不为空
    , NOT_NULL,
    // 为null
    IS_NULL
    // 分组
    ,GROUP_BY
    // 排序
    ,ORDER_BY
}
