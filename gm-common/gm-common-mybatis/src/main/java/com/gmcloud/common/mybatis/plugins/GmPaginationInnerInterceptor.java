package com.gmcloud.common.mybatis.plugins;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.ParameterUtils;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.dialects.IDialect;
import com.google.common.base.Objects;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.sql.SQLException;

/**
 * 分页拦截器
 * <p>
 * 重构分页插件, 当 size 小于 0 时, 直接设置为 0, 防止错误查询全表
 *
 * @author zhangsan
 * @since  2022-08-17
 */
public class GmPaginationInnerInterceptor extends PaginationInnerInterceptor {

	/**
	 * 数据库类型
	 * <p>
	 * 查看 {@link #findIDialect(Executor)} 逻辑
	 */
	private DbType dbType;

	/**
	 * 方言实现类
	 * <p>
	 * 查看 {@link #findIDialect(Executor)} 逻辑
	 */
	private IDialect dialect;

	public GmPaginationInnerInterceptor(DbType dbType) {
		this.dbType = dbType;
	}

	public GmPaginationInnerInterceptor(IDialect dialect) {
		this.dialect = dialect;
	}

	public GmPaginationInnerInterceptor(){}

	@Override
	public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds,
			ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
		IPage<?> page = ParameterUtils.findPage(parameter).orElse(null);
		// size 小于 0 直接设置为 0 , 即不查询任何数据
		if (null != page && page.getSize() < 0) {
			page.setSize(0);
		}
		super.beforeQuery(executor, ms, page, rowBounds, resultHandler, boundSql);
	}

	@Override
	public DbType getDbType() {
		return dbType;
	}

	@Override
	public void setDbType(DbType dbType) {
		this.dbType = dbType;
	}

	@Override
	public IDialect getDialect() {
		return dialect;
	}

	@Override
	public void setDialect(IDialect dialect) {
		this.dialect = dialect;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		GmPaginationInnerInterceptor that = (GmPaginationInnerInterceptor) o;
		return dbType == that.dbType && Objects.equal(dialect, that.dialect);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(super.hashCode(), dbType, dialect);
	}
}
