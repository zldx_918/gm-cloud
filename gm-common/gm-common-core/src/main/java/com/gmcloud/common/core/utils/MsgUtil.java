package com.gmcloud.common.core.utils;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * i18n 工具类
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:22
 */
public class MsgUtil {

    private MsgUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 通过code 获取中文错误信息
     * @param code 代码
     * @return 消息
     */
    public static String getMessage(String code) {
        MessageSource messageSource = SpringUtil.getBean("messageSource");
        return messageSource.getMessage(code, null, Locale.CHINA);
    }

    /**
     * 通过code 和参数获取中文错误信息
     * @param code 代码
     * @return 消息
     */
    public static String getMessage(String code, Object... objects) {
        MessageSource messageSource = SpringUtil.getBean("messageSource");
        return messageSource.getMessage(code, objects, Locale.CHINA);
    }

}
