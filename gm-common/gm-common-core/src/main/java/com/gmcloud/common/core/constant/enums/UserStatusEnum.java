package com.gmcloud.common.core.constant.enums;


/**
 * 用户内置状态
 */
public enum UserStatusEnum implements IBaseEnum<String,String> {

    //    状态：1启用、0禁用,2锁定，3密码过期，4，已注销
    STATUS_DISABLE("0", "禁用"),
    STATUS_NORMAL("1", "正常"),
    STATUS_LOCK("2", "锁定"),
    STATUS_PASSWORD_EXPIRATION("3", "密码过期"),
    STATUS_LOGGED_OUT("4", "已注销");


    /**
     * 类型
     */
    private final String type;

    /**
     * 描述
     */
    private final String description;


    UserStatusEnum(String type, String description) {
        this.type = type;
        this.description = description;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
