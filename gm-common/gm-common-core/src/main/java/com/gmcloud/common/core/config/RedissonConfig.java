package com.gmcloud.common.core.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author zl.sir
 * @since  2022/9/22 12:50
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.core.config
 * @description 基于redis分布式锁配置
 */
@AutoConfiguration
public class RedissonConfig {

    private final RedisProperties redisProperties;

    public RedissonConfig(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
    }

    @Bean
    public RedissonClient getRedisson(){

        Config config = new Config();
        //单机模式  依次设置redis地址和密码
        config.useSingleServer().
                setAddress("redis://" + redisProperties.getHost() + ":" + redisProperties.getPort()).
                setPassword(redisProperties.getPassword());
        return Redisson.create(config);
    }
}
