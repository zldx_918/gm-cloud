package com.gmcloud.common.core.exception;

import java.io.Serial;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 22:41
 */
public class ValidateCodeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = -7285211528095468156L;

    public ValidateCodeException() {
    }

    public ValidateCodeException(String msg) {
        super(msg);
    }

}

