package com.gmcloud.common.core.constant;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 22:47
 */
public final class CacheConstants {

   private CacheConstants() {
      throw new IllegalStateException("Utility class");
   }
    /**
     * oauth 缓存前缀
     */
    public static final String PROJECT_OAUTH_ACCESS = "token::access_token";

    /**
     * oauth 缓存令牌前缀
     */
    public static final String PROJECT_OAUTH_TOKEN = "gm_oauth:token:";

    /**
     * 验证码前缀
     */
    public static final String DEFAULT_CODE_KEY = "DEFAULT_CODE_KEY:";

    /**
     * 菜单信息缓存
     */
    public static final String MENU_DETAILS = "menu_details";

    /**
     * 用户信息缓存
     */
    public static final String USER_DETAILS = "user_details";

    /**
     * 字典信息缓存
     */
    public static final String DICT_DETAILS = "dict_details";

    /**
     * oauth 客户端信息
     */
    public static final String CLIENT_DETAILS_KEY = "client:details";

    /**
     * 参数缓存
     */
    public static final String PARAMS_DETAILS = "params_details";

}
