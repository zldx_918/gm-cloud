package com.gmcloud.common.core.constant.enums;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 17:13
 */
public enum MenuTypeEnum implements IBaseEnum<Integer,String>{

    /**
     * 菜单目录
     */
    LEFT_MENU(0, "dir"),

    /**
     * 菜单页面
     */
    TOP_MENU(1, "menu"),

    /**
     * 页面按钮
     */
    BUTTON(2, "button");

    /**
     * 类型
     */
    private final Integer type;

    /**
     * 描述
     */
    private final String description;


    MenuTypeEnum(Integer type, String description) {
        this.type = type;
        this.description = description;
    }

    @Override
    public Integer getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
