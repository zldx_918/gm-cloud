package com.gmcloud.common.core.constant;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 18:
 *  服务名称
 */
public final class ServiceNameConstants {

    private ServiceNameConstants() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 认证服务的SERVICEID
     */
    public static final String AUTH_SERVICE = "gm-auth";

    /**
     * UMPS模块
     */
    public static final String UMPS_SERVICE = "gm-upms-biz";
}
