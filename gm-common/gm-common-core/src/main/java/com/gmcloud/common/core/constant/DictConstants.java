package com.gmcloud.common.core.constant;

/**
 * 字典常量
 */
public final class DictConstants {

    private DictConstants() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 用户状态
     */
    public static final String USER_STATUS="user_status";
}
