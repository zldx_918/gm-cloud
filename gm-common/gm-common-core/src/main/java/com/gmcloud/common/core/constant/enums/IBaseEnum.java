package com.gmcloud.common.core.constant.enums;

public interface IBaseEnum<K,M> {

    /**
     *
     * @return 类型
     */
    K getType();

    /**
     *
     * @return 详情
     */
    M getDescription();

}
