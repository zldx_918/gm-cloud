package com.gmcloud.common.core.exception;

import java.io.Serial;

public class BaseException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -9032304515460274352L;

    public BaseException() {
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
