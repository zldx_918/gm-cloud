package com.gmcloud.common.core.utils;

import com.gmcloud.common.core.constant.enums.IBaseEnum;

import java.util.HashMap;
import java.util.Map;

public class EnumUtil extends cn.hutool.core.util.EnumUtil {


    private EnumUtil() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * @param clazz 枚举class
     * @return map
     * @description 真正的工具类 - 将枚举转为Map使用，只要参数 clazz 继承了IBaseEnum
     * eg: Map<String, String> map = EnumUtil.generateConvertToMap(UserStatusEnum.class);
     */
    public static <K, V> Map<K, V> generateConvertToMap(Class<? extends IBaseEnum<K, V>> clazz) {
        Map<K, V> mapResult = new HashMap<>();

        if (!clazz.isEnum()) {
            throw new UnsupportedOperationException("参数不合法：非枚举类，不支持转换，请检查程序是否有误！");
        }

        // 通过class.getEnumConstants();获取所有的枚举字段和值
        IBaseEnum<K, V>[] iBaseEnums = clazz.getEnumConstants();
        for (IBaseEnum<K, V> iBaseEnum : iBaseEnums) {
            mapResult.put(iBaseEnum.getType(), iBaseEnum.getDescription());
        }
        return mapResult;

    }
}
