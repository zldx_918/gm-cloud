package com.gmcloud.common.core.exception;

import java.io.Serial;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 21:59
 */
public class CheckedException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public CheckedException(String message) {
        super(message);
    }

    public CheckedException(Throwable cause) {
        super(cause);
    }

    public CheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}


