package com.gmcloud.common.security.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.google.common.base.Objects;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

import java.io.Serial;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 16:03
 */
public class GmUser extends User implements OAuth2AuthenticatedPrincipal {

    @Serial
    private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 手机号
     */
    private String phone;


    private Boolean admin;

    private final Map<String, Object> attributes = new HashMap<>();

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 角色
     */
    private List<String> roles;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 头像
     */
    private String avatarName;

    /**
     * 生日
     */
    private LocalDateTime birthday;

    /**
     * 性别
     */
    private String gender;

    public GmUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.attributes;
    }


    @Override
    public String getName() {
        return this.getUsername();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GmUser gmUser = (GmUser) o;
        return Objects.equal(id, gmUser.id) && Objects.equal(phone, gmUser.phone) && Objects.equal(admin, gmUser.admin) && Objects.equal(attributes, gmUser.attributes) && Objects.equal(nickName, gmUser.nickName) && Objects.equal(roles, gmUser.roles) && Objects.equal(email, gmUser.email) && Objects.equal(avatarName, gmUser.avatarName);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), id, phone, admin, attributes, nickName, roles, email, avatarName);
    }
}
