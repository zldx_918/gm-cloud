package com.gmcloud.common.security.config.oauth2;

import java.util.ArrayList;
import java.util.List;

/**
 * 不需要拦截的配置
 * @author zl.sir
 * @version 1.0
 * @since 2021/1/25 13:12
 */
public class Ignore {
    /**
     * 需要忽略的 URL 格式，不考虑请求方法
     */
    private List<String> pattern = new ArrayList<>();

    /**
     * 需要忽略的 GET 请求
     */
    private List<String> get = new ArrayList<>();

    /**
     * 需要忽略的 POST 请求
     */
    private List<String> post =new ArrayList<>();

    /**
     * 需要忽略的 DELETE 请求
     */
    private List<String> delete = new ArrayList<>();

    /**
     * 需要忽略的 PUT 请求
     */
    private List<String> put = new ArrayList<>();

    /**
     * 需要忽略的 HEAD 请求
     */
    private List<String> head = new ArrayList<>();

    /**
     * 需要忽略的 PATCH 请求
     */
    private List<String> patch =new ArrayList<>();

    /**
     * 需要忽略的 OPTIONS 请求
     */
    private List<String> options = new ArrayList<>();

    /**
     * 需要忽略的 TRACE 请求
     */
    private List<String> trace = new ArrayList<>();


    public List<String> getPattern() {
        return pattern;
    }

    public void setPattern(List<String> pattern) {
        this.pattern = pattern;
    }

    public List<String> getGet() {
        return get;
    }

    public void setGet(List<String> get) {
        this.get = get;
    }

    public List<String> getPost() {
        return post;
    }

    public void setPost(List<String> post) {
        this.post = post;
    }

    public List<String> getDelete() {
        return delete;
    }

    public void setDelete(List<String> delete) {
        this.delete = delete;
    }

    public List<String> getPut() {
        return put;
    }

    public void setPut(List<String> put) {
        this.put = put;
    }

    public List<String> getHead() {
        return head;
    }

    public void setHead(List<String> head) {
        this.head = head;
    }

    public List<String> getPatch() {
        return patch;
    }

    public void setPatch(List<String> patch) {
        this.patch = patch;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public List<String> getTrace() {
        return trace;
    }

    public void setTrace(List<String> trace) {
        this.trace = trace;
    }

}
