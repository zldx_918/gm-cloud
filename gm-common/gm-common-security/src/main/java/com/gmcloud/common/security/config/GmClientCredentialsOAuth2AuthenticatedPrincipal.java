package com.gmcloud.common.security.config;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;

import java.util.Collection;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/23 21:45
 */
public class GmClientCredentialsOAuth2AuthenticatedPrincipal implements OAuth2AuthenticatedPrincipal {

    private final Map<String, Object> attributes;

    private final Collection<GrantedAuthority> authorities;

    private final String name;

    public GmClientCredentialsOAuth2AuthenticatedPrincipal(Map<String, Object> attributes, Collection<GrantedAuthority> authorities, String name) {
        this.attributes = attributes;
        this.authorities = authorities;
        this.name = name;
    }

    @Override
    public Map<String, Object> getAttributes() {
        return this.attributes;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getName() {
        return this.name;
    }
}