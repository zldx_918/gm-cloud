package com.gmcloud.common.job.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * xxl-job配置
 *
 * @author zl.sir
 * @since 2020/9/14
 */

@ConfigurationProperties(prefix = "xxl.job")
public class XxlJobProperties {

	@NestedConfigurationProperty
	private XxlAdminProperties admin = new XxlAdminProperties();

	@NestedConfigurationProperty
	private XxlExecutorProperties executor = new XxlExecutorProperties();


	public XxlAdminProperties getAdmin() {
		return admin;
	}

	public void setAdmin(XxlAdminProperties admin) {
		this.admin = admin;
	}

	public XxlExecutorProperties getExecutor() {
		return executor;
	}

	public void setExecutor(XxlExecutorProperties executor) {
		this.executor = executor;
	}
}
