package com.gmcloud.common.datasource.constant;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/23 18:00
 * 数据源相关常量
 */
public final class DataSourceConstants {

    private DataSourceConstants() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * 数据源名称
     */
    public static final String DS_NAME = "name";

    /**
     * 默认数据源（master）
     */
    public static final String DS_MASTER = "master";

    /**
     * jdbcurl
     */
    public static final String DS_JDBC_URL = "url";

    /**
     * 用户名
     */
    public static final String DS_USER_NAME = "username";

    /**
     * 密码
     */
    public static final String DS_USER_PWD = "password";

    /**
     * 驱动包名称
     */
    public static final String DS_DRIVER_CLASS_NAME = "driver_class_name";


}
