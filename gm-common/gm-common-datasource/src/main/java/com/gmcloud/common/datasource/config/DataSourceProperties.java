package com.gmcloud.common.datasource.config;


/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/23 17:53
 * 数据源配置
 */
public class DataSourceProperties extends org.springframework.boot.autoconfigure.jdbc.DataSourceProperties {

    /**
     * 查询数据源的SQL
     */
    private String queryDsSql = "select * from gm_datasource_conf where del_flag = 0";


    public String getQueryDsSql() {
        return queryDsSql;
    }

    public void setQueryDsSql(String queryDsSql) {
        this.queryDsSql = queryDsSql;
    }
}
