

## 系统说明

-  **分布式事务**


### seata 分布式事务解决方案
##### 部署 seata-server 服务端

`下载
seata-server-1.5.1.zip
(82.9 MB)
并解压
image.png
切换至 bin 目录 ，启动 seata 服务
启动 seata 服务
`
`Shell

# windows
seata-server.bat -p 8091 -h 127.0.0.1 -m file

# mac or linux
sh seata-server.sh -p 8091 -h 127.0.0.1 -m file`

##### 微服务客户端接入

##### ① 添加依赖

涉及分布式的业务模块 加入 common-seata
XML

<dependency>
<groupId>com.gmcloud</groupId>
<artifactId>gmg-common-seata</artifactId>
</dependency>

##### ②  服务调用方声明 GlobalTransactional

Java

`@GlobalTransactional // 分布式事务注解
@Transactional(rollbackFor = Exception)
public R consumer() {
feign.providerMethod()  // 通过feign 调用 服务提供方的接口
}`

##### ③  微服务涉及数据库添加 undo_log 表

SQL

``CREATE TABLE `undo_log` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`branch_id` bigint(20) NOT NULL,
`xid` varchar(100) NOT NULL,
`context` varchar(128) NOT NULL,
`rollback_info` longblob NOT NULL,
`log_status` int(11) NOT NULL,
`log_created` datetime NOT NULL,
`log_modified` datetime NOT NULL,
`ext` varchar(100) DEFAULT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `ux_undo_log` (`xid`,`branch_id`)
) ENGINE=InnoDB AU``