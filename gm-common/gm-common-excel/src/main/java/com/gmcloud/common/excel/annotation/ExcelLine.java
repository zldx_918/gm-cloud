package com.gmcloud.common.excel.annotation;

import java.lang.annotation.*;

/**
 * @author zl
 * @since  2022/9/20 21:13
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.annotation
 * @description excel line
 */
@Documented
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelLine {
}
