package com.gmcloud.common.excel.vo;

import java.util.HashSet;
import java.util.Set;

/**
 * @author zl
 * @since  2022/9/20 20:54
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.vo
 * @description 校验错误消息实体
 */
public class ErrorMessage {
    /**
     * 行号
     */
    private Long lineNum;

    /**
     * 错误信息
     */
    private Set<String> errors = new HashSet<>();

    public ErrorMessage(Long lineNum, Set<String> errors) {
        this.lineNum = lineNum;
        this.errors = errors;
    }


    public ErrorMessage(String error) {
        HashSet<String> objects = new HashSet<>();
        objects.add(error);
        this.errors = objects;
    }

    public Long getLineNum() {
        return lineNum;
    }

    public void setLineNum(Long lineNum) {
        this.lineNum = lineNum;
    }

    public Set<String> getErrors() {
        return errors;
    }

    public void setErrors(Set<String> errors) {
        this.errors = errors;
    }
}
