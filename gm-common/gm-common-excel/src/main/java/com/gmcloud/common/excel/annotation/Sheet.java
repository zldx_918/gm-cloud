package com.gmcloud.common.excel.annotation;

import com.gmcloud.common.excel.head.HeadGenerator;

import java.lang.annotation.*;

/**
 * @author zl
 * @since  2022/9/20 19:10
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.annotation
 * @description @sheet 注解
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Sheet {

    int sheetNo() default -1;

    /**
     * sheet name
     */
     String sheetName() default "sheet1";

    /**
     * 包含字段
     */
    String[] includes() default {};

    /**
     * 排除字段
     */
    String[] excludes() default {};

    /**
     * 头生成器
     */
    Class<? extends HeadGenerator> headGenerateClass() default HeadGenerator.class;
}
