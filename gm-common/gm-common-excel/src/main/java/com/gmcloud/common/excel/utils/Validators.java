package com.gmcloud.common.excel.utils;

import javax.validation.*;
import java.util.Set;

/**
 * @author zl
 * @since  2022/9/20 20:59
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.utils
 * @description 校验工具类
 */
public final class Validators {

    private static final Validator VALIDATOR;

    private Validators() {
    }

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    /**
     * Validates all constraints on {@code object}.
     * @param object object to validate
     * @param <T> the type of the object to validate
     * @return constraint violations or an empty set if none
     * @throws IllegalArgumentException if object is {@code null} or if {@code null} is
     * passed to the varargs groups
     * @throws ValidationException if a non recoverable error happens during the
     * validation process
     */
    public static <T> Set<ConstraintViolation<T>> validate(T object) {
        return VALIDATOR.validate(object);
    }
}
