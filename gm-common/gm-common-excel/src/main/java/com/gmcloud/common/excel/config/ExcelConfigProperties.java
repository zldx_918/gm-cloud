package com.gmcloud.common.excel.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author zl
 * @since  2022/9/20 19:51
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.config
 * @description excel 配置类
 */
@ConfigurationProperties(prefix = ExcelConfigProperties.PREFIX)
public class ExcelConfigProperties {

     static final String PREFIX = "excel";

     /**
      * 模板路径
      */
     private String templatePath = "excel";


     public String getTemplatePath() {
          return templatePath;
     }

     public void setTemplatePath(String templatePath) {
          this.templatePath = templatePath;
     }
}
