package com.gmcloud.common.excel;

import com.gmcloud.common.excel.aspest.DynamicNameAspect;
import com.gmcloud.common.excel.config.ExcelConfigProperties;
import com.gmcloud.common.excel.handler.RequestExcelArgumentResolver;
import com.gmcloud.common.excel.handler.ResponseExcelReturnValueHandler;
import com.gmcloud.common.excel.processor.NameProcessor;
import com.gmcloud.common.excel.processor.NameSpelExpressionProcessor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zl
 * @since  2022/9/20 19:49
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common
 * @description excel 配置类
 */
@AutoConfiguration
@Import(ExcelHandlerConfiguration.class)
@EnableConfigurationProperties(ExcelConfigProperties.class)
public class ExcelAutoConfiguration {


    /**
     * 内部参数解析器,解析参数和返回结果
     */
    private final RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    private final ResponseExcelReturnValueHandler responseExcelReturnValueHandler;


    public ExcelAutoConfiguration(RequestMappingHandlerAdapter requestMappingHandlerAdapter, ResponseExcelReturnValueHandler responseExcelReturnValueHandler) {
        this.requestMappingHandlerAdapter = requestMappingHandlerAdapter;
        this.responseExcelReturnValueHandler = responseExcelReturnValueHandler;
    }


    /**
     * 追加 Excel 请求处理器 到 springmvc 中,使用@RequestExcel注解会进入#RequestExcelArgumentResolver
     */
    @PostConstruct
    public void setRequestExcelArgumentResolver() {
        List<HandlerMethodArgumentResolver> argumentResolvers = requestMappingHandlerAdapter.getArgumentResolvers();
        List<HandlerMethodArgumentResolver> resolverList = new ArrayList<>();
        resolverList.add(new RequestExcelArgumentResolver());
        assert argumentResolvers != null;
        resolverList.addAll(argumentResolvers);
        requestMappingHandlerAdapter.setArgumentResolvers(resolverList);
    }

    /**
     * 追加 Excel 响应处理器 到 springmvc 中
     */
    @PostConstruct
    public void setReturnValueHandlers() {
        List<HandlerMethodReturnValueHandler> returnValueHandlers = requestMappingHandlerAdapter.getReturnValueHandlers();

        List<HandlerMethodReturnValueHandler> newHandlers = new ArrayList<>();
        newHandlers.add(responseExcelReturnValueHandler);
        assert returnValueHandlers != null;
        newHandlers.addAll(returnValueHandlers);
        requestMappingHandlerAdapter.setReturnValueHandlers(newHandlers);
    }


    /**
     * SPEL 解析处理器
     *
     * @return NameProcessor excel名称解析器
     */
    @Bean
    @ConditionalOnMissingBean
    public NameProcessor nameProcessor() {
        return new NameSpelExpressionProcessor();
    }

    /**
     * Excel名称解析处理切面
     *
     * @param nameProcessor SPEL 解析处理器
     * @return DynamicNameAspect
     */
    @Bean
    @ConditionalOnMissingBean
    public DynamicNameAspect dynamicNameAspect(NameProcessor nameProcessor) {
        return new DynamicNameAspect(nameProcessor);
    }


}
