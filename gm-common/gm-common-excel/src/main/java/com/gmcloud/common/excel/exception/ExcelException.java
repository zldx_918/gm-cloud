package com.gmcloud.common.excel.exception;

/**
 * @author zl
 * @since  2022/9/20 22:33
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.exception
 * @description excel 异常
 */
public class ExcelException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public ExcelException(String message) {
        super(message);
    }
}
