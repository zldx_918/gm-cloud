package com.gmcloud.common.excel.service;


import java.util.List;

/**
 * @author zl
 * @since  2022/9/21 17:16
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.test.excel.service
 * @description service
 */
public interface ExcelService<T> {

    /**
     * 保存excel导入数据,batchInsert,所有数据一次性插入

     * @param dtos
     */
    void batchInsertExcel(List<T> dtos);
}
