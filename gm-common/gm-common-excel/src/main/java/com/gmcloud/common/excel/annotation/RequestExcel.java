package com.gmcloud.common.excel.annotation;

import com.gmcloud.common.excel.listener.DefaultAnalysisEventListener;
import com.gmcloud.common.excel.listener.ListAnalysisEventListener;

import java.lang.annotation.*;

/**
 * @author zl
 * @since  2022/9/20 20:48
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.annotation
 * @description 上传excel注解
 */

@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestExcel {
    /**
     * 前端上传字段名称 file
     */
    String fileName() default "file";

    /**
     * 读取的监听器类
     *
     * @return readListener
     */
    Class<? extends ListAnalysisEventListener> readListener() default DefaultAnalysisEventListener.class;

    /**
     * 是否跳过空行
     *
     * @return 默认跳过
     */
    boolean ignoreEmptyRow() default false;

    /**
     * 默认存储服务
     * @return
     */
    Class<?> saveService()  default Class.class;
}
