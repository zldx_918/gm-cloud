package com.gmcloud.common.excel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.util.ListUtils;
import com.gmcloud.common.utils.JsonUtil;
import com.gmcloud.common.excel.annotation.ExcelLine;
import com.gmcloud.common.excel.service.ExcelService;
import com.gmcloud.common.excel.utils.SpringContextHolder;
import com.gmcloud.common.excel.vo.ErrorMessage;
import com.gmcloud.common.excel.utils.Validators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author zl
 * @since  2022/9/20 20:50
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.handler
 * @description 默认读取excel事件监听
 */
public class DefaultAnalysisEventListener<T> extends ListAnalysisEventListener<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultAnalysisEventListener.class);

    private List<T> cachedDataList = new ArrayList<>();

    private final List<ErrorMessage> errorMessageList = new ArrayList<>();

    private Long lineNum = 1L;


    /**
     * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 100;

    /**
     * 存储服务
     */
    private ExcelService<T> excelService;

    @Override
    public List<T> getList() {
        return cachedDataList;
    }

    @Override
    public List<ErrorMessage> getErrors() {
        return errorMessageList;
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param analysisContext analysisContext
     */
    @Override
    public void invoke(T data, AnalysisContext analysisContext) {
        LOGGER.info("解析到一条数据:{}", JsonUtil.objToString(data));

        lineNum++;
        // 验证对象所有字段
        Set<ConstraintViolation<Object>> violations = Validators.validate(data);
        //  获取验证错误消息
        if (!CollectionUtils.isEmpty(violations)) {
            Set<String> messageSet = violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet());
            errorMessageList.add(new ErrorMessage(lineNum,messageSet));
        } else {
            Field[] fields = data.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(ExcelLine.class) && field.getType() == Long.class) {
                    //  设置可以访问private变量的变量值
                    field.setAccessible(true);
                    try {
                        field.setLong(data, lineNum);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            cachedDataList.add(data);

            // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
            if (cachedDataList.size() >= BATCH_COUNT) {

                saveData();
                // 存储完成清理 list
                cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
            }
        }


    }

    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param analysisContext analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }

    /**
     * 加上存储数据库
     */
    private void saveData() {
        LOGGER.info("{}条数据，开始存储数据库！", cachedDataList.size());
        if (Objects.nonNull(excelService)) {
            excelService.batchInsertExcel(cachedDataList);
        }
        LOGGER.info("存储数据库成功！");
    }

    @Override
    public void setSaveService(Class<?> saveService) {
        this.excelService = (ExcelService<T>) SpringContextHolder.getBean(saveService);
    }
}
