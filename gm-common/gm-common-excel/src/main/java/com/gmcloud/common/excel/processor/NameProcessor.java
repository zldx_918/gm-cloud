package com.gmcloud.common.excel.processor;

import java.lang.reflect.Method;

/**
 * @author zl
 * @since  2022/9/20 19:55
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.processor
 * @description 名称解析
 */
public interface NameProcessor {
    /**
     * 解析名称
     * @param args 拦截器对象
     * @param method
     * @param key 表达式
     * @return
     */
    String doDetermineName(Object[] args, Method method, String key);
}
