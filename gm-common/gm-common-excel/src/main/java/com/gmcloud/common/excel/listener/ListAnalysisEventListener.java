package com.gmcloud.common.excel.listener;

import com.alibaba.excel.event.AnalysisEventListener;
import com.gmcloud.common.excel.vo.ErrorMessage;

import java.util.List;

/**
 * @author zl
 * @since  2022/9/20 20:50
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.handler
 * @description 读取监听
 */
public abstract class ListAnalysisEventListener<T> extends AnalysisEventListener<T> {

    /**
     * 获取 excel 解析的对象列表
     *
     * @return 集合
     */
    public abstract List<T> getList();

    /**
     * 获取异常校验结果
     *
     * @return 集合
     */
    public abstract List<ErrorMessage> getErrors();

    /**
     * 设置存储服务
     * @param saveService 存储服务
     */
    public abstract void setSaveService(Class<?> saveService) ;
}
