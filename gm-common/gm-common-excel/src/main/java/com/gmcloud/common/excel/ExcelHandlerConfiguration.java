package com.gmcloud.common.excel;

import com.alibaba.excel.converters.Converter;
import com.gmcloud.common.excel.config.ExcelConfigProperties;
import com.gmcloud.common.excel.enhance.DefaultWriterBuilderEnhancer;
import com.gmcloud.common.excel.enhance.WriterBuilderEnhancer;
import com.gmcloud.common.excel.handler.ManySheetWriteHandler;
import com.gmcloud.common.excel.handler.ResponseExcelReturnValueHandler;
import com.gmcloud.common.excel.handler.SheetWriteHandler;
import com.gmcloud.common.excel.handler.SingleSheetWriteHandler;
import com.gmcloud.common.excel.head.I18nHeaderCellWriteHandler;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author zl
 * @since  2022/9/20 19:50
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common
 * @description excel 事件配置
 */
public class ExcelHandlerConfiguration {

    private final ExcelConfigProperties configProperties;

    private final ObjectProvider<List<Converter<?>>> converterProvider;

    public ExcelHandlerConfiguration(ExcelConfigProperties configProperties, ObjectProvider<List<Converter<?>>> converterProvider) {
        this.configProperties = configProperties;
        this.converterProvider = converterProvider;
    }

    /**
     * ExcelBuild增强
     *
     * @return DefaultWriterBuilderEnhancer 默认什么也不做的增强器
     */
    @Bean
    @ConditionalOnMissingBean
    public WriterBuilderEnhancer writerBuilderEnhancer() {
        return new DefaultWriterBuilderEnhancer();
    }


    /**
     * 单sheet 写入处理器
     */
    @Bean
    @ConditionalOnMissingBean
    public SingleSheetWriteHandler singleSheetWriteHandler(WriterBuilderEnhancer writerBuilderEnhancer) {
        return new SingleSheetWriteHandler(configProperties, converterProvider, writerBuilderEnhancer);
    }

    /**
     * 多sheet 写入处理器
     */
    @Bean
    @ConditionalOnMissingBean
    public ManySheetWriteHandler manySheetWriteHandler(WriterBuilderEnhancer writerBuilderEnhancer) {
        return new ManySheetWriteHandler(configProperties, converterProvider, writerBuilderEnhancer);
    }

    /**
     * 返回Excel文件的 response 处理器
     * @param sheetWriteHandlerList 页签写入处理器集合
     * @return ResponseExcelReturnValueHandler
     */
    @Bean
    @ConditionalOnMissingBean
    public ResponseExcelReturnValueHandler responseExcelReturnValueHandler(
            List<SheetWriteHandler> sheetWriteHandlerList) {
        return new ResponseExcelReturnValueHandler(sheetWriteHandlerList);
    }

    /**
     * excel 头的国际化处理器
     * @param messageSource 国际化源
     */
    @Bean
    @ConditionalOnBean(MessageSource.class)
    @ConditionalOnMissingBean
    public I18nHeaderCellWriteHandler i18nHeaderCellWriteHandler(MessageSource messageSource) {
        return new I18nHeaderCellWriteHandler(messageSource);
    }

}
