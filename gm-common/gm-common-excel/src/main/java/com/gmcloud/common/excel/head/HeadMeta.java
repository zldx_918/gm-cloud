package com.gmcloud.common.excel.head;

import java.util.List;
import java.util.Set;

/**
 * @author zl
 * @since  2022/9/20 19:13
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.head
 * @description head信息
 */
public class HeadMeta {
    /**
     * <p>
     * 自定义头部信息
     * </p>
     * 实现类根据数据的class信息，定制Excel头<br/>
     * 具体方法使用参考：https://www.yuque.com/easyexcel/doc/write#b4b9de00
     */
    private List<List<String>> head;

    /**
     * 忽略头对应字段名称
     */
    private Set<String> ignoreHeadFields;


    public List<List<String>> getHead() {
        return head;
    }

    public void setHead(List<List<String>> head) {
        this.head = head;
    }

    public Set<String> getIgnoreHeadFields() {
        return ignoreHeadFields;
    }

    public void setIgnoreHeadFields(Set<String> ignoreHeadFields) {
        this.ignoreHeadFields = ignoreHeadFields;
    }
}
