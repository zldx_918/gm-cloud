package com.gmcloud.common.excel.handler;

import com.gmcloud.common.excel.annotation.ResponseExcel;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author zl
 * @since  2022/9/20 19:05
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.common.handler
 * @description sheet写处理器
 */
public interface SheetWriteHandler<T> {

    /**
     * 是否支持
     *
     * @param obj obj
     * @return boolean
     */
    boolean support(T obj);

    /**
     * 校验
     *
     * @param responseExcel 注解
     */
    void check(ResponseExcel responseExcel);

    /**
     * 返回的对象
     *
     * @param o             obj
     * @param response      输出对象
     * @param responseExcel 注解
     * @throws IOException IO
     */
    void export(T o, HttpServletResponse response, ResponseExcel responseExcel) throws IOException;

    /**
     * 写入对象
     *
     * @param o             obj
     * @param response      输出对象
     * @param responseExcel 注解
     * @throws IOException IO
     */
    void write(T o, HttpServletResponse response, ResponseExcel responseExcel) throws IOException;
}
