package com.gmcloud.common.log;

import com.gmcloud.common.log.aspect.SysLogAspect;
import com.gmcloud.common.log.event.SysLogListener;
import com.gmcloud.common.log.interceptor.DataLogInterceptor;
import com.gmcloud.upms.api.system.feign.RemoteLogService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 12:24
 * 日志自动配置
 */
@Configuration(proxyBeanMethods = false)
@EnableAsync
@EnableConfigurationProperties(DataLogConfiguration.class)
@ConditionalOnWebApplication   //当前项目是web项目
public class LogAutoConfiguration {

    @Bean
    public SysLogListener sysLogListener(RemoteLogService remoteLogService) {
        return new SysLogListener(remoteLogService);
    }

    @Bean
    public SysLogAspect sysLogAspect() {
        return new SysLogAspect();
    }


    @Bean
    public DataLogInterceptor dataLogInterceptor(DataLogConfiguration dataLogConfiguration) {
        return new DataLogInterceptor(dataLogConfiguration);
    }
}
