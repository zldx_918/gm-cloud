package com.gmcloud.common.log;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "data-log")
public class DataLogConfiguration {

    private boolean enabled=true;//是否开启全部实体操作日志
    private boolean includeInsert=false;//是否包含插入日志

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isIncludeInsert() {
        return includeInsert;
    }

    public void setIncludeInsert(boolean includeInsert) {
        this.includeInsert = includeInsert;
    }

}
