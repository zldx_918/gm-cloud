package com.gmcloud.common.log.event;

import com.gmcloud.upms.api.system.entity.SysLog;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 12:45
 * 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {

    @Serial
    private static final long serialVersionUID = -5420113675301553018L;

    public SysLogEvent(SysLog source) {
        super(source);
    }

}
