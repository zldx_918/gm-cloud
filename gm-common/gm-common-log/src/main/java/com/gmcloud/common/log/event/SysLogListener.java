package com.gmcloud.common.log.event;

import com.gmcloud.common.core.constant.SecurityConstants;
import com.gmcloud.upms.api.system.entity.SysDataLog;
import com.gmcloud.upms.api.system.entity.SysLog;
import com.gmcloud.upms.api.system.feign.RemoteLogService;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 12:47
 * 异步监听日志事件
 */
public class SysLogListener {

    private final RemoteLogService remoteLogService;

    public SysLogListener(RemoteLogService remoteLogService) {
        this.remoteLogService = remoteLogService;
    }

    /**
     * 监听日志并且保存到数据库
     * @param event 事件
     */
    @Async
    @Order
    @EventListener(SysLogEvent.class)
    public void saveSysLog(SysLogEvent event) {
        SysLog sysLog = (SysLog) event.getSource();
        remoteLogService.saveLog(sysLog, SecurityConstants.FROM_IN);
    }

    /**
     * 监听日志并且保存到数据库
     * @param event 事件
     */
    @Async
    @Order
    @EventListener(SysDataLogEvent.class)
    public void saveSysDataLog(SysDataLogEvent event) {
        SysDataLog sysLog = (SysDataLog) event.getSource();
        remoteLogService.saveDataLog(sysLog, SecurityConstants.FROM_IN);
    }
}
