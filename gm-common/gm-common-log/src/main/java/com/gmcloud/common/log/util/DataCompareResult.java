package com.gmcloud.common.log.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataCompareResult {

    private Long id;
    private LinkedHashMap<String,Object[]> dataChange;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, Object[]> getDataChange() {
        return dataChange;
    }

    public void setDataChange(Map<String, Object[]> dataChange) {
        this.dataChange = (LinkedHashMap<String, Object[]>) dataChange;
    }

}
