package com.gmcloud.common.log.annotation;

import java.lang.annotation.*;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 12:34
 * 操作日志注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SysLog {

    /**
     * 描述
     *
     * @return {String}
     */
    String value() default "";

    /**
     * spel 表达式
     *
     * @return 日志描述
     */
    String expression() default "";
}
