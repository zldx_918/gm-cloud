package com.plugin.crawler.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 18:56
 */
@SpringBootApplication
public class GmCrawlerDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmCrawlerDataApplication.class, args);
    }
}
