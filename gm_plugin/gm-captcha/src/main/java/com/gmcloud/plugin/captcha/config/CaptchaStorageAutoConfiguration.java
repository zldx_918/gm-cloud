package com.gmcloud.plugin.captcha.config;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 22:02
 */

import com.gmcloud.plugin.captcha.service.CaptchaCacheService;
import com.gmcloud.plugin.captcha.service.CaptchaServiceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 存储策略自动配置
 */
@Configuration(proxyBeanMethods = false)
public class CaptchaStorageAutoConfiguration {


    @Bean(name = "captchaCacheService")
    public CaptchaCacheService captchaCacheService(CaptchaProperties captchaProperties){
        //缓存类型redis/local/....
        return CaptchaServiceFactory.getCache(captchaProperties.getCacheSupport().name());
    }
}
