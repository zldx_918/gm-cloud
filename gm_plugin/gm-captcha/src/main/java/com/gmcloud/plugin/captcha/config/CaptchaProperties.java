package com.gmcloud.plugin.captcha.config;

import com.gmcloud.plugin.captcha.model.CaptchaTypeEnum;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.awt.*;
import java.util.Objects;

import static com.gmcloud.plugin.captcha.config.CaptchaProperties.StorageType.local;


/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 21:16
 * 验证码配置类
 */
@ConfigurationProperties("captcha")
public class CaptchaProperties {

    /**
     * 验证码支持类型.
     */
    private CaptchaTypeEnum support = CaptchaTypeEnum.DEFAULT;

    /**
     * 滑动拼图底图路径.
     */
    private String jigsaw = "";

    /**
     * 点选文字底图路径.
     */
    private String picClick = "";


    /**
     * 右下角水印文字(我的水印).
     */
    private String waterMark = "我的水印";

    /**
     * 右下角水印字体(文泉驿正黑).
     */
    private String waterFont = "WenQuanZhengHei.ttf";

    /**
     * 点选文字验证码的文字字体(文泉驿正黑).
     */
    private String fontType = "WenQuanZhengHei.ttf";

    /**
     * 校验滑动拼图允许误差偏移量(默认5像素).
     */
    private String slipOffset = "5";

    /**
     * aes加密坐标开启或者禁用(true|false).
     */
    private Boolean aesStatus = true;

    /**
     * 滑块干扰项(0/1/2)
     */
    private String interferenceOptions = "0";

    /**
     * local缓存的阈值
     */
    private String cacheNumber = "1000";

    /**
     * 定时清理过期local缓存(单位秒)
     */
    private String timingClear = "180";

    /**
     * 缓存类型redis/local/....
     */
    private StorageType cacheSupport = local;
    /**
     * 历史数据清除开关
     */
    private boolean historyDataClearEnable = false;

    /**
     * 一分钟内接口请求次数限制 开关
     */
    private boolean reqFrequencyLimitEnable = false;

    /***
     * 一分钟内check接口失败次数
     */
    private int reqGetLockLimit = 5;
    /**
     *
     */
    private int reqGetLockSeconds = 300;

    /***
     * get接口一分钟内限制访问数
     */
    private int reqGetMinuteLimit = 100;
    private int reqCheckMinuteLimit = 100;
    private int reqVerifyMinuteLimit = 100;

    /**
     * 点选字体样式
     */
    private int fontStyle = Font.BOLD;

    /**
     * 点选字体大小
     */
    private int fontSize = 25;

    /**
     * 点选文字个数，存在问题，暂不要使用
     */
    private int clickWordCount = 4;


    public enum StorageType {
        /**
         * 内存.
         */
        local,
        /**
         * redis.
         */
        redis,
        /**
         * 其他.
         */
        other,
    }

    public CaptchaTypeEnum getSupport() {
        return support;
    }

    public void setSupport(CaptchaTypeEnum support) {
        this.support = support;
    }

    public String getJigsaw() {
        return jigsaw;
    }

    public void setJigsaw(String jigsaw) {
        this.jigsaw = jigsaw;
    }

    public String getPicClick() {
        return picClick;
    }

    public void setPicClick(String picClick) {
        this.picClick = picClick;
    }

    public String getWaterMark() {
        return waterMark;
    }

    public void setWaterMark(String waterMark) {
        this.waterMark = waterMark;
    }

    public String getWaterFont() {
        return waterFont;
    }

    public void setWaterFont(String waterFont) {
        this.waterFont = waterFont;
    }

    public String getFontType() {
        return fontType;
    }

    public void setFontType(String fontType) {
        this.fontType = fontType;
    }

    public String getSlipOffset() {
        return slipOffset;
    }

    public void setSlipOffset(String slipOffset) {
        this.slipOffset = slipOffset;
    }

    public Boolean getAesStatus() {
        return aesStatus;
    }

    public void setAesStatus(Boolean aesStatus) {
        this.aesStatus = aesStatus;
    }

    public String getInterferenceOptions() {
        return interferenceOptions;
    }

    public void setInterferenceOptions(String interferenceOptions) {
        this.interferenceOptions = interferenceOptions;
    }

    public String getCacheNumber() {
        return cacheNumber;
    }

    public void setCacheNumber(String cacheNumber) {
        this.cacheNumber = cacheNumber;
    }

    public String getTimingClear() {
        return timingClear;
    }

    public void setTimingClear(String timingClear) {
        this.timingClear = timingClear;
    }

    public StorageType getCacheSupport() {
        return cacheSupport;
    }

    public void setCacheSupport(StorageType cacheSupport) {
        this.cacheSupport = cacheSupport;
    }

    public boolean isHistoryDataClearEnable() {
        return historyDataClearEnable;
    }

    public void setHistoryDataClearEnable(boolean historyDataClearEnable) {
        this.historyDataClearEnable = historyDataClearEnable;
    }

    public boolean isReqFrequencyLimitEnable() {
        return reqFrequencyLimitEnable;
    }

    public void setReqFrequencyLimitEnable(boolean reqFrequencyLimitEnable) {
        this.reqFrequencyLimitEnable = reqFrequencyLimitEnable;
    }

    public int getReqGetLockLimit() {
        return reqGetLockLimit;
    }

    public void setReqGetLockLimit(int reqGetLockLimit) {
        this.reqGetLockLimit = reqGetLockLimit;
    }

    public int getReqGetLockSeconds() {
        return reqGetLockSeconds;
    }

    public void setReqGetLockSeconds(int reqGetLockSeconds) {
        this.reqGetLockSeconds = reqGetLockSeconds;
    }

    public int getReqGetMinuteLimit() {
        return reqGetMinuteLimit;
    }

    public void setReqGetMinuteLimit(int reqGetMinuteLimit) {
        this.reqGetMinuteLimit = reqGetMinuteLimit;
    }

    public int getReqCheckMinuteLimit() {
        return reqCheckMinuteLimit;
    }

    public void setReqCheckMinuteLimit(int reqCheckMinuteLimit) {
        this.reqCheckMinuteLimit = reqCheckMinuteLimit;
    }

    public int getReqVerifyMinuteLimit() {
        return reqVerifyMinuteLimit;
    }

    public void setReqVerifyMinuteLimit(int reqVerifyMinuteLimit) {
        this.reqVerifyMinuteLimit = reqVerifyMinuteLimit;
    }

    public int getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public int getClickWordCount() {
        return clickWordCount;
    }

    public void setClickWordCount(int clickWordCount) {
        this.clickWordCount = clickWordCount;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CaptchaProperties that = (CaptchaProperties) o;
        return historyDataClearEnable == that.historyDataClearEnable && reqFrequencyLimitEnable == that.reqFrequencyLimitEnable && reqGetLockLimit == that.reqGetLockLimit && reqGetLockSeconds == that.reqGetLockSeconds && reqGetMinuteLimit == that.reqGetMinuteLimit && reqCheckMinuteLimit == that.reqCheckMinuteLimit && reqVerifyMinuteLimit == that.reqVerifyMinuteLimit && fontStyle == that.fontStyle && fontSize == that.fontSize && clickWordCount == that.clickWordCount && support == that.support && Objects.equals(jigsaw, that.jigsaw) && Objects.equals(picClick, that.picClick) && Objects.equals(waterMark, that.waterMark) && Objects.equals(waterFont, that.waterFont) && Objects.equals(fontType, that.fontType) && Objects.equals(slipOffset, that.slipOffset) && Objects.equals(aesStatus, that.aesStatus) && Objects.equals(interferenceOptions, that.interferenceOptions) && Objects.equals(cacheNumber, that.cacheNumber) && Objects.equals(timingClear, that.timingClear) && cacheSupport == that.cacheSupport;
    }

    @Override
    public int hashCode() {
        return Objects.hash(support, jigsaw, picClick, waterMark, waterFont, fontType, slipOffset, aesStatus, interferenceOptions, cacheNumber, timingClear, cacheSupport, historyDataClearEnable, reqFrequencyLimitEnable, reqGetLockLimit, reqGetLockSeconds, reqGetMinuteLimit, reqCheckMinuteLimit, reqVerifyMinuteLimit, fontStyle, fontSize, clickWordCount);
    }
}
