package com.gmcloud.plugin.captcha.model;

import java.text.MessageFormat;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 23:14
 */
public enum RepCodeEnum {
    /** 0001 - 0099 网关应答码 */
    SUCCESS(200, "成功"),
    ERROR(501, "操作失败"),
    EXCEPTION(500, "服务器内部异常"),

    BLANK_ERROR(4001, "{0}不能为空"),
    NULL_ERROR(4002, "{0}不能为空"),
    NOT_NULL_ERROR(4003, "{0}必须为空"),
    NOT_EXIST_ERROR(4004, "{0}数据库中不存在"),
    EXIST_ERROR(4005, "{0}数据库中已存在"),
    PARAM_TYPE_ERROR(4006, "{0}类型错误"),
    PARAM_FORMAT_ERROR(4007, "{0}格式错误"),

    API_CAPTCHA_INVALID(4008, "验证码已失效，请重新获取"),
    API_CAPTCHA_COORDINATE_ERROR(4009, "验证失败"),
    API_CAPTCHA_ERROR(4010, "获取验证码失败,请联系管理员"),
    API_CAPTCHA_BASEMAP_NULL(4011, "底图未初始化成功，请检查路径"),

    API_REQ_LIMIT_GET_ERROR(4012, "get接口请求次数超限，请稍后再试!"),
    API_REQ_INVALID(4013, "无效请求，请重新获取验证码"),
    API_REQ_LOCK_GET_ERROR(4014, "接口验证失败数过多，请稍后再试"),
    API_REQ_LIMIT_CHECK_ERROR(4015, "check接口请求次数超限，请稍后再试!"),
    API_REQ_LIMIT_VERIFY_ERROR(4016, "verify请求次数超限!"),
    ;
    private Integer code;
    private String desc;

    RepCodeEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
    public Integer getCode() {
        return code;
    }
    public String getDesc() {
        return desc;
    }
    public String getName(){
        return this.name();
    }

    /** 将入参fieldNames与this.desc组合成错误信息
     *  {fieldName}不能为空
     * @param fieldNames
     * @return
     */
    public ResponseModel parseError(Object... fieldNames) {
        ResponseModel errorMessage=new ResponseModel();
        String newDesc = MessageFormat.format(this.desc, fieldNames);

        errorMessage.setCode(this.code);
        errorMessage.setMessage(newDesc);
        return errorMessage;
    }
}
