package com.gmcloud.plugin.captcha.service.impl;

import com.gmcloud.common.core.utils.RedisUtil;
import com.gmcloud.common.core.utils.SpringContextHolder;
import com.gmcloud.plugin.captcha.service.CaptchaCacheService;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/31 4:26
 * redis支持
 * 对于分布式部署的应用，我们建议应用自己实现CaptchaCacheService，比如用Redis。
 * 如果应用是单点的，也没有使用redis，那默认使用内存。
 * 内存缓存只适合单节点部署的应用，否则验证码生产与验证在节点之间信息不同步，导致失败。
 *
 */
public class CaptchaCacheServiceRedisImpl implements CaptchaCacheService {

    private static final RedisUtil REDIS_UTIL = SpringContextHolder.getBean("redisUtils");

    @Override
    public void set(String key, String value, long expiresInSeconds) {
        REDIS_UTIL.set(key, value, expiresInSeconds, TimeUnit.SECONDS);
    }

    @Override
    public boolean exists(String key) {
        return REDIS_UTIL.hasKey(key);
    }

    @Override
    public void delete(String key) {
        REDIS_UTIL.del(Collections.singletonList(key));
    }

    @Override
    public String get(String key) {
        return (String) REDIS_UTIL.get(key);
    }

    @Override
    public Long increment(String key, long val) {
        return REDIS_UTIL.getRedisTemplate().opsForValue().increment(key,val);
    }

    @Override
    public String support() {
        return "redis";
    }
}
