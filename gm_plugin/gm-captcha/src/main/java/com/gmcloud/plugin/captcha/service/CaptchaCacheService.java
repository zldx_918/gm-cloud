package com.gmcloud.plugin.captcha.service;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 22:45
 * 验证码缓存接口
 */
public interface CaptchaCacheService {
    void set(String key, String value, long expiresInSeconds);

    boolean exists(String key);

    void delete(String key);

    String get(String key);

    /**
     * 缓存类型-local/redis/memcache/..
     * 通过java SPI机制，接入方可自定义实现类
     *
     * @return
     */
    String support();

    /***
     *
     * @param key
     * @param val
     * @return  long
     */
    default Long increment(String key, long val) {
        return 0L;
    }
}
