package com.gmcloud.plugin.captcha.model;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/31 0:03
 */
@Schema(description = "响应")
public class ResponseModel<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 8445617032523881407L;

    @Schema(description = "code")
    private Integer code;
    @Schema(description = "消息")
    private String message;
    @Schema(description = "结果")
    private T result;

    public ResponseModel() {
        this.code = RepCodeEnum.SUCCESS.getCode();
    }

    public ResponseModel(RepCodeEnum repCodeEnum) {
        this.setRepCodeEnum(repCodeEnum);
    }

    //成功
    public static ResponseModel success() {
        return ResponseModel.successMsg("成功");
    }

    public static ResponseModel successMsg(String message) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setMessage(message);
        return responseModel;
    }

    public static ResponseModel successData(Object data) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setCode(RepCodeEnum.SUCCESS.getCode());
        responseModel.setMessage(RepCodeEnum.SUCCESS.getDesc());
        responseModel.setResult(data);
        return responseModel;
    }

    //失败
    public static ResponseModel errorMsg(RepCodeEnum message) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setRepCodeEnum(message);
        return responseModel;
    }

    public static ResponseModel errorMsg(String message) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setCode(RepCodeEnum.ERROR.getCode());
        responseModel.setMessage(message);
        return responseModel;
    }

    public static ResponseModel errorMsg(RepCodeEnum repCodeEnum, String message) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setCode(repCodeEnum.getCode());
        responseModel.setMessage(message);
        return responseModel;
    }

    public static ResponseModel exceptionMsg(String message) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setCode(RepCodeEnum.EXCEPTION.getCode());
        responseModel.setMessage(RepCodeEnum.EXCEPTION.getDesc() + ": " + message);
        return responseModel;
    }

    @Override
    public String toString() {
        return "ResponseModel{" + "code='" + code + '\'' + ", message='"
                + message + '\'' + ", repData=" + result + '}';
    }

    public boolean isSuccess() {
        return code.equals(RepCodeEnum.SUCCESS.getCode());
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setRepCodeEnum(RepCodeEnum repCodeEnum) {
        this.code = repCodeEnum.getCode();
        this.message = repCodeEnum.getDesc();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
