package com.gmcloud.plugin.captcha;

import com.gmcloud.plugin.captcha.config.CaptchaServiceAutoConfiguration;
import com.gmcloud.plugin.captcha.config.CaptchaStorageAutoConfiguration;
import com.gmcloud.plugin.captcha.controller.CaptchaController;
import com.gmcloud.plugin.captcha.config.CaptchaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/30 21:26
 */
@Configuration
@EnableConfigurationProperties(CaptchaProperties.class)
@Import({CaptchaServiceAutoConfiguration.class, CaptchaStorageAutoConfiguration.class, CaptchaController.class})
public class CaptchaAutoConfiguration {


}
