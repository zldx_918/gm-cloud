package com.gmcloud.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 17:08
 */

@EnableDiscoveryClient
@SpringBootApplication
public class GmGatewayApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(GmGatewayApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(GmGatewayApplication.class, args);

        Environment env = application.getEnvironment();
        String host = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");

        String info = "----------------------------------------------------------\n\t"
                .concat("应用 '{}' is running! Access URLs:\n\t")
                .concat("本地地址: \t\thttp://localhost:{}\n\t")
                .concat("网络地址: \thttp://{}:{}\n\t")
                .concat("SwaggerDoc: \thttp://{}:{}/swagger-ui.html\n\t")
                .concat("----------------------------------------------------------");
        LOGGER.info(info,
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                host, port,
                host, port);
    }
}
