package com.gmcloud.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;


import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/16 17:20
 * <p>
 *     全局拦截器，作用所有的微服务
 * </p>
 * <p>
 *     1. 对请求的API调用过滤，记录接口的请求时间，方便日志审计、告警、分析等运维操作 2. 后期可以扩展对接其他日志系统
 * </p>
 */
@Component
public class ApiLoggingFilter implements GlobalFilter, Ordered {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiLoggingFilter.class);
    private static final String START_TIME = "startTime";

    private static final String X_REAL_IP = "X-Real-IP";// nginx需要配置


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        if (LOGGER.isDebugEnabled()) {
            HttpMethod httpMethod= Optional.ofNullable(exchange.getRequest().getMethod()).orElse(HttpMethod.OPTIONS);
            String info = String.format("Method:{%s} Host:{%s} Path:{%s} Query:{%s}",
                    httpMethod.name(), exchange.getRequest().getURI().getPort(),
                    exchange.getRequest().getURI().getPath(), exchange.getRequest().getQueryParams());
            LOGGER.info(info);
        }
        exchange.getAttributes().put(START_TIME, System.currentTimeMillis());

        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            Long startTime = exchange.getAttribute(START_TIME);
            if (Objects.nonNull(startTime)) {
                Long executeTime = System.currentTimeMillis() - startTime;


                List<String> ips = exchange.getRequest().getHeaders().get(X_REAL_IP);
                String ip = ips != null ? ips.get(0) : null;
                String api = exchange.getRequest().getURI().getRawPath();

                int code = 500;
                if (Objects.nonNull(exchange.getResponse().getStatusCode())) {
                    code = exchange.getResponse().getStatusCode().value();
                }

                // 当前仅记录日志，后续可以添加日志队列，来过滤请求慢的接口
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("来自IP地址：{}的请求接口：{}，响应状态码：{}，请求耗时：{}ms", ip, api, code, executeTime);
                }
            }
        }));
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }
}
