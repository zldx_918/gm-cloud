package com.gmcloud.gateway.filter;

import com.gmcloud.gateway.config.SpringDocConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/16 19:55
 */
public class SwaggerBasicGatewayFilter implements GlobalFilter {


    private static final Logger LOGGER = LoggerFactory.getLogger(SwaggerBasicGatewayFilter.class);
    private static final String API_URI = "/v3/api-docs";

    private static final String BASIC_PREFIX = "Basic ";

    private final SpringDocConfiguration.SwaggerDocProperties swaggerProperties;

    public SwaggerBasicGatewayFilter(SpringDocConfiguration.SwaggerDocProperties swaggerProperties) {
        this.swaggerProperties = swaggerProperties;
    }


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();

        if (!request.getURI().getPath().contains(API_URI)) {
            return chain.filter(exchange);
        }

        if (hasAuth(request)) {
            return chain.filter(exchange);
        } else {
            ServerHttpResponse response = exchange.getResponse();
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            response.getHeaders().add(HttpHeaders.WWW_AUTHENTICATE, "Basic Realm=\"gm\"");
            return response.setComplete();
        }
    }

    /**
     * 简单的basic认证
     *
     * @param request  request
     * @return 是否有权限
     */
    private boolean hasAuth(ServerHttpRequest request) {
        String auth = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
        LOGGER.info("Basic认证信息为：{}", auth);
        if (!StringUtils.hasText(auth) || !auth.startsWith(BASIC_PREFIX)) {
            return Boolean.FALSE;
        }

        String username = swaggerProperties.getBasic().getUsername();
        String password = swaggerProperties.getBasic().getPassword();

        String encodeToString = Base64Utils
                .encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));

        return auth.equals(BASIC_PREFIX + encodeToString);
    }
}
