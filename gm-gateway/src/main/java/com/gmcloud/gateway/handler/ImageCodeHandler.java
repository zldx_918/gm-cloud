package com.gmcloud.gateway.handler;


import com.gmcloud.common.utils.R;
import com.gmcloud.plugin.captcha.service.CaptchaService;
import com.gmcloud.plugin.captcha.vo.CaptchaVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/16 20:08
 * 验证码生成逻辑处理类
 */

//@Service
public class ImageCodeHandler implements HandlerFunction<ServerResponse> {

    /**
     * 验证码服务
     */
    private final CaptchaService captchaService;

    public ImageCodeHandler(CaptchaService captchaService) {
        this.captchaService = captchaService;
    }


    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest) {

        assert serverRequest.remoteAddress() != null;
        Optional<CaptchaVO> vo = serverRequest.bodyToMono(CaptchaVO.class).flatMap(body -> Mono.just(body)).blockOptional();


        return ServerResponse.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(Mono.just(R.ok()), R.class);
    }
}
