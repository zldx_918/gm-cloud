package com.gmcloud.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 17:26
 * 网关配置
 */
@RefreshScope
@ConfigurationProperties("gateway")
public class GatewayConfigProperties {

    /**
     * 网关解密登录前端密码 秘钥 {@link com.gmcloud}
     */
    private String encodeKey;

    /**
     * 网关不需要校验验证码的客户端 {@link com.gmcloud.gateway.filter.ValidateCodeGatewayFilter}
     */
    private List<String> ignoreClients;


    public String getEncodeKey() {
        return encodeKey;
    }

    public void setEncodeKey(String encodeKey) {
        this.encodeKey = encodeKey;
    }

    public List<String> getIgnoreClients() {
        return ignoreClients;
    }

    public void setIgnoreClients(List<String> ignoreClients) {
        this.ignoreClients = ignoreClients;
    }
}
