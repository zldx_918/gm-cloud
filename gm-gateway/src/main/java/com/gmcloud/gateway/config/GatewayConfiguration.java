package com.gmcloud.gateway.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmcloud.gateway.filter.GmRequestGlobalFilter;
import com.gmcloud.gateway.filter.PasswordDecoderFilter;
import com.gmcloud.gateway.filter.SwaggerBasicGatewayFilter;
import com.gmcloud.gateway.handler.GlobalExceptionHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 网关配置
 *
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/15 18:18
 * Full模式下通过方法调用指向的仍旧是原来的Bean,lite模式下，直接返回新实例对象。true是FULL模式
 */
@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(GatewayConfigProperties.class)
public class GatewayConfiguration {


    @Bean
    public PasswordDecoderFilter passwordDecoderFilter(GatewayConfigProperties configProperties) {
        return new PasswordDecoderFilter(configProperties);
    }
    @Bean
    public GmRequestGlobalFilter pigRequestGlobalFilter() {
        return new GmRequestGlobalFilter();
    }

    /**
     * 网关全局异常处理
     * @param objectMapper mapper
     * @return 异常
     */
    @Bean
    public GlobalExceptionHandler globalExceptionHandler(ObjectMapper objectMapper) {
        return new GlobalExceptionHandler(objectMapper);
    }

    @Bean
    @ConditionalOnProperty(name = "swagger.basic.enabled")
    public SwaggerBasicGatewayFilter swaggerBasicGatewayFilter(
            SpringDocConfiguration.SwaggerDocProperties swaggerProperties) {
        return new SwaggerBasicGatewayFilter(swaggerProperties);
    }
}
