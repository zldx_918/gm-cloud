package com.gmcloud.gateway.config;

import com.gmcloud.gateway.handler.ImageCodeHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/16 20:06
 * 路由配置信息,验证码路由 ,暂时不用，使用web请求
 */
//@Configuration(proxyBeanMethods = false)
public class RouterFunctionConfiguration {


    private final ImageCodeHandler imageCodeHandler;

    public RouterFunctionConfiguration(ImageCodeHandler imageCodeHandler) {
        this.imageCodeHandler = imageCodeHandler;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction() {
        return RouterFunctions.route(
                RequestPredicates.path("/code").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)), imageCodeHandler);
    }
}
