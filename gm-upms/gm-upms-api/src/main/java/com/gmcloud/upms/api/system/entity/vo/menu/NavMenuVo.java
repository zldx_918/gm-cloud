package com.gmcloud.upms.api.system.entity.vo.menu;

/**
 * @author zl
 * @since  2022/10/20 10:51
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.api.system.vo.menu
 * @description 返回导航菜单路由
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Schema(description ="导航菜单输出")
public class NavMenuVo {

    @Schema(description = "菜单名称")
    private String name;

    @Schema(description = "路由地址")
    private String path;




    @Schema(description ="转发地址")
    private String redirect;

    @Schema(description = "组件地址")
    private String component;

    /**
     * Layout 布局容器  ParentView 父级视图
     */
    @Schema(description = "布局容器")
    private String layout;


    @Schema(description = "路由meta")
    private MenuMetaVo meta;

    @Schema(description = "子路由")
    private List<NavMenuVo> children;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }


    public MenuMetaVo getMeta() {
        return meta;
    }

    public void setMeta(MenuMetaVo meta) {
        this.meta = meta;
    }

    public List<NavMenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<NavMenuVo> children) {
        this.children = children;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }
}
