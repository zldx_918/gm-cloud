package com.gmcloud.upms.api.system.entity.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 16:28
 */
@Schema(description = "token输出对象")
public class TokenVo {

    @Schema(description = "id")
    private String id;

    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "客户端id")
    private String clientId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "认证token")
    private String accessToken;

    @Schema(description = "")
    private Instant issuedAt;

    @Schema(description = "过期")
    private Instant expiresAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Instant getIssuedAt() {
        return issuedAt;
    }

    public void setIssuedAt(Instant issuedAt) {
        this.issuedAt = issuedAt;
    }

    public Instant getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Instant expiresAt) {
        this.expiresAt = expiresAt;
    }
}
