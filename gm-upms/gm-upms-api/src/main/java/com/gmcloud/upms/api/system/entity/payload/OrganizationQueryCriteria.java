package com.gmcloud.upms.api.system.entity.payload;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 17:08
 * @description 组织查询
 */
@Schema(description = "组织查询参数")
public class OrganizationQueryCriteria {
}
