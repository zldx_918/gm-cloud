package com.gmcloud.upms.api.system.feign;

import com.gmcloud.common.core.constant.SecurityConstants;
import com.gmcloud.common.core.constant.ServiceNameConstants;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.dto.UserInfo;
import com.gmcloud.upms.api.system.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 18:07
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.UMPS_SERVICE
        , fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService {

    /**
     * 通过用户名查询用户、角色信息
     *
     * @param username 用户名
     * @param from     调用标志
     * @return R
     */
    @GetMapping("/user/info/{username}")
    R<UserInfo> info(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM) String from);

}
