package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.google.common.base.Objects;

import java.io.Serial;
import java.io.Serializable;

/**
 * 角色表
 *
 * @author zl.sir
 * @since 2022-08-26
 */
@TableName("sys_role")
public class SysRole extends BaseEntity implements Serializable {

	@Serial
    private static final long serialVersionUID = 4310028141234875416L;
	/**
	 * ID
	 */
	@TableId(value = "role_id",type = IdType.AUTO)
	private Long id;

	/**
	 * 名称
	 */
 	@TableField("role_name")
	private String roleName;

	/**
	 * 角色代码
	 */
 	@TableField("role_code")
	private String roleCode;

	/**
	 * 角色级别
	 */
 	@TableField("level")
	private Integer level;

	/**
	 * 描述
	 */
 	@TableField("description")
	private String description;

	/**
	 * 数据权限
	 */
 	@TableField("data_scope")
	private String dataScope;

	/**
	 * 1激活，0禁用
	 */
 	@TableField("enabled")
	private Boolean enabled;


    /**
     * 删除标识（0-正常,1-删除）
     */
    @TableLogic
	private Integer delFlag;


    @TableField(exist = false)
    private Long userId;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = roleName;
  }


  public String getRoleCode() {
    return roleCode;
  }

  public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
  }


  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }


  public String getDataScope() {
    return dataScope;
  }

  public void setDataScope(String dataScope) {
    this.dataScope = dataScope;
  }


  public Boolean getEnabled() {
    return enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }


    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SysRole sysRole = (SysRole) o;
        return Objects.equal(id, sysRole.id) && Objects.equal(roleName, sysRole.roleName) && Objects.equal(roleCode, sysRole.roleCode) && Objects.equal(level, sysRole.level) && Objects.equal(description, sysRole.description) && Objects.equal(dataScope, sysRole.dataScope) && Objects.equal(enabled, sysRole.enabled)   && Objects.equal(delFlag, sysRole.delFlag) && Objects.equal(userId, sysRole.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, roleName, roleCode, level, description, dataScope, enabled, delFlag, userId);
    }
}
