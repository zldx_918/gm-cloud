package com.gmcloud.upms.api.system.entity.dto;

import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.util.List;

@Schema(description = "用户入参")
public class SysUserDto {

    @Schema(description = "id")
    @NotNull(message = "用户id不能为空", groups = {BaseEntity.Update.class, BaseEntity.Delete.class})
    private Long id;

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    @NotNull(message = "用户名不能为空", groups = BaseEntity.Insert.class)
    private String username;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 修改
     * 状态：1启用、0禁用,2锁定，3密码过期，4，已注销
     */
    @NotNull(message = "用户状态不能为空", groups = BaseEntity.Insert.class)
    @Schema(description = "状态：1启用、0禁用,2锁定，3密码过期，4，已注销,最终以字典表为准")
    private String status;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 修改
     * 头像
     */
    @Schema(description = "头像地址")
    private String avatar;

    @Schema(description = "昵称")
    private String nickName;

    /**
     * 性别
     */
    @NotNull(message = "性别不能为空", groups = BaseEntity.Insert.class)
    @Schema(description = "性别")
    private String gender;

    /**
     * 邮箱
     */
    @Schema(description = "邮箱")
    private String email;

    /**
     * 生日
     */
    @Schema(description = "生日")
    private String birthday;

    /**
     * 是否管理员
     */
    @Schema(description = "是否管理员")
    private Boolean admin;


    @Schema(description = "角色列表")
    private List<SysRoleDto> roles;

    private List<SysOrganizationDto> organizations;

    private List<SysPostDto> posts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public List<SysRoleDto> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRoleDto> roles) {
        this.roles = roles;
    }

    public List<SysOrganizationDto> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<SysOrganizationDto> organizations) {
        this.organizations = organizations;
    }

    public List<SysPostDto> getPosts() {
        return posts;
    }

    public void setPosts(List<SysPostDto> posts) {
        this.posts = posts;
    }
}
