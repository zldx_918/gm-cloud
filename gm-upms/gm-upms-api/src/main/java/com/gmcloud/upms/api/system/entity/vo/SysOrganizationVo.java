package com.gmcloud.upms.api.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 16:26
 * @description 组织vo
 */
@Schema(description = "组织vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysOrganizationVo extends BaseEntity {


    @Serial
    private static final long serialVersionUID = 9174223831016250624L;
    /**
     * 单位id
     */
    @Schema(description = "单位id")
    private Long id;

    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    private String organizationName;

    /**
     * 组织地址
     */
    @Schema(description = "组织地址")
    private String organizationAddress;

    /**
     * 组织电话
     */
    @Schema(description = "组织电话")
    private String organizationTel;

    /**
     * 上级组织
     */
    @Schema(description = "上级组织")
    private Long pid;

    /**
     * 下级组织数
     */
    @Schema(description = "下级组织数")
    private Long subCount;

    /**
     * 排序,数字越小越排前
     */
    @Schema(description = "排序,数字越小越排前")
    private Long organizationSort;

    /**
     * 组织状态,1启用，0禁用
     */
    @Schema(description = "组织状态,1启用，0禁用")
    private Boolean enabled;

    @Schema(description = "组织类型，1是组织机构,2是部门")
    private String type;


    @Schema(description = "下级节点")
    private List<SysOrganizationVo> children = new ArrayList<>();


    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAddress() {
        return organizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public String getOrganizationTel() {
        return organizationTel;
    }

    public void setOrganizationTel(String organizationTel) {
        this.organizationTel = organizationTel;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getSubCount() {
        return subCount;
    }

    public void setSubCount(Long subCount) {
        this.subCount = subCount;
    }

    public Long getOrganizationSort() {
        return organizationSort;
    }

    public void setOrganizationSort(Long organizationSort) {
        this.organizationSort = organizationSort;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<SysOrganizationVo> getChildren() {
        return children;
    }

    public void setChildren(List<SysOrganizationVo> children) {
        this.children = children;
    }

    /**
     * 树形菜单显示
     * @return 组织名称
     */
    public String getLabel() {
        return organizationName;
    }

    public Long getValue() {
        return id;
    }
}
