package com.gmcloud.upms.api.system.factory;

import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.dto.UserInfo;
import com.gmcloud.upms.api.system.feign.RemoteUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * @author zl
 * @since  2022/9/18 12:45
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.api.system.factory
 * @description    RemoteUserService服务降级处理
 */
public class RemoteUserFallbackFactory implements FallbackFactory<RemoteUserService> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteUserFallbackFactory.class);

    @Override
    public RemoteDemoServiceFallbackImpl create(Throwable cause) {
        RemoteDemoServiceFallbackImpl remoteDemoServiceFallback=new RemoteDemoServiceFallbackImpl();
        remoteDemoServiceFallback.setCause(cause);
        return remoteDemoServiceFallback;
    }


    /**
     * 接口降级实现 降级逻辑实现
     */
    public class RemoteDemoServiceFallbackImpl implements RemoteUserService {

        private Throwable cause;

        @Override
        public R<UserInfo> info(String username, String from) {
            return null;
        }

        public void setCause(Throwable cause) {
            this.cause = cause;
        }
    }
}
