package com.gmcloud.upms.api.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * @author zl
 * @version 1.0
 * @since 2023-03-16 11:07
 * @description 字典vo
 */
@Schema(description = "字典vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysDictVo  extends BaseEntity {

    /**
     * ID
     */
    @Schema(description = "字典id")
    private Long id;

    /**
     * 字典名称
     */
    @Schema(description = "字典名称")
    private String dictName;

    /**
     * 描述
     */
    @Schema(description = "字典描述")
    private String description;


    /**
     * 状态
     */
    @Schema(description = "字典状态", example = "0是禁用,1是启用")
    private Boolean enabled;

    private Boolean system;
    /**
     * 字典详情对应
     */
    @Schema(description = "字典详情对应关系")
    private List<SysDictDetailVo> dictDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<SysDictDetailVo> getDictDetails() {
        return dictDetails;
    }

    public void setDictDetails(List<SysDictDetailVo> dictDetails) {
        this.dictDetails = dictDetails;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }
}