package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.google.common.base.Objects;

import java.io.Serial;

/**
 * 岗位
 *
 * @author zl.sir
 * @since 2022-08-26
 */
@TableName("sys_post")
public class SysPost extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 41464780675348237L;
    /**
     * ID
     */
    @TableId(value = "post_id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(exist = false)
    private Long userId;
    /**
     * 岗位编码
     */
    @TableField("post_code")
    private String postCode;

    /**
     * 岗位名称
     */
    @TableField("post_name")
    private String postName;

    /**
     * 岗位状态
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 排序
     */
    @TableField("post_sort")
    private Integer postSort;

    /**
     * 备注
     */
    @TableField("remark")
    private Integer remark;


    /**
     * 删除标识（0-正常,1-删除）
     */
    @TableLogic
    private Integer delFlag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }


    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }


    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }


    public Integer getPostSort() {
        return postSort;
    }

    public void setPostSort(Integer postSort) {
        this.postSort = postSort;
    }


    public Integer getRemark() {
        return remark;
    }

    public void setRemark(Integer remark) {
        this.remark = remark;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SysPost sysPost = (SysPost) o;
        return Objects.equal(id, sysPost.id) && Objects.equal(postCode, sysPost.postCode) && Objects.equal(postName, sysPost.postName) && Objects.equal(enabled, sysPost.enabled) && Objects.equal(postSort, sysPost.postSort) && Objects.equal(remark, sysPost.remark) && Objects.equal(delFlag, sysPost.delFlag);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, postCode, postName, enabled, postSort, remark, delFlag);
    }
}
