package com.gmcloud.upms.api.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.gmcloud.upms.api.system.entity.vo.menu.SysMenuVo;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serial;
import java.util.List;

/**
 * @author zl
 * @since  2022/10/20 11:05
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.api.system.vo
 * @description 角色信息输出
 */
@Schema(description = "角色vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysRoleVo extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 2129029449651917393L;
    /**
     * ID
     */
    @Schema(description = "角色id")
    private Long id;

    /**
     * 名称
     */
    @Schema(description =  "角色名称")
    private String roleName;

    /**
     * 角色级别
     */
    @Schema(description =  "角色级别")
    private Integer level;

    /**
     * 描述
     */
    @Schema(description =  "描述")
    private String description;

    /**
     * 数据权限
     */
    @Schema(description = "角色范围")
    private String dataScope;

    @Schema(description =  "是否启用,1启用，0禁用")
    private Boolean enabled;


    @Schema(description =  "关联菜单")
    private List<SysMenuVo> menus;

    @Schema(description =  "关联菜单ids")
    private List<Long> menuIds;

    @Schema(description = "用户id，转换使用")
    @JsonIgnore
    private Long userId;


    @Schema(description = "关联api的ids")
    private List<Long> apiIds;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }


    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return enabled;
    }


    @Schema(description =  "是否禁用")
    public Boolean getState() {
        return Boolean.TRUE.equals(enabled);
    }


    @Schema(description =  "下拉选项显示名称")
    public String getLabel() {
        return roleName;
    }

    @Schema(description =  "返回值")
    public Long getValue() {
        return id;
    }

    @Schema(description =  "是否禁用，true禁用")
    public Boolean getDisabled() {
        return Boolean.FALSE.equals(enabled);
    }

    public List<SysMenuVo> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenuVo> menus) {
        this.menus = menus;
    }

    public List<Long> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(List<Long> menuIds) {
        this.menuIds = menuIds;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
