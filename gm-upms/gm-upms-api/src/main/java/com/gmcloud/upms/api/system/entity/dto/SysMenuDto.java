package com.gmcloud.upms.api.system.entity.dto;

import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;

public class SysMenuDto {

    @Schema(description = "id")
    @NotNull(message = "菜单id不能为空", groups = {BaseEntity.Update.class, BaseEntity.Delete.class})
    private Long id;


    /**
     * 上级菜单ID,为空就是布局，0,目录,1,路由,2 按钮
     */
    @Schema(description = "pid")
    private Long pid;

    /**
     * 菜单类型
     */
    @Schema(description = "type")
    @NotNull(message = "菜单类型不能为空", groups = {BaseEntity.Insert.class})
    private Long type;

    /**
     * 菜单标题
     */
    @Schema(description = "title")
    @NotNull(message = "菜单标题不能为空",groups = {BaseEntity.Insert.class})
    private String title;

    /**
     * 组件名称
     */
    @Schema(description = "name")
    private String name;

    /**
     * 组件
     */
    @Schema(description = "component")
    private String component;

    /**
     * 排序
     */
    @Schema(description = "menu_sort")
    private Long menuSort;

    /**
     * 图标
     */
    @Schema(description = "icon")
    private String icon;

    /**
     * 链接地址
     */
    @Schema(description = "path")
    private String path;

    /**
     * 是否外链
     */
    @Schema(description = "frame")
    private Boolean frame;

    /**
     * 缓存
     */
    @Schema(description = "cache")
    private Boolean cache;

    /**
     * 隐藏
     */
    @Schema(description = "hidden")
    private Boolean hidden;

    /**
     * 权限
     */
    @Schema(description = "permission")
    private String permission;

    /**
     * 是否需要验证
     */
    @Schema(description = "是否需要验证")
    private Boolean requiresAuth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Long getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(Long menuSort) {
        this.menuSort = menuSort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getFrame() {
        return frame;
    }

    public void setFrame(Boolean frame) {
        this.frame = frame;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Boolean getRequiresAuth() {
        return requiresAuth;
    }

    public void setRequiresAuth(Boolean requiresAuth) {
        this.requiresAuth = requiresAuth;
    }
}
