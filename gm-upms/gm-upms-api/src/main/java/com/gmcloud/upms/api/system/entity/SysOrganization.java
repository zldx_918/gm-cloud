package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;

import java.io.Serial;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 16:32
 * @description 单位组织
 */
@TableName("sys_organization")
public class SysOrganization extends BaseEntity {

    @Serial
    private static final long serialVersionUID = 6496605868976329962L;
    /**
     * 单位id
     */
    @TableId(value = "organization_id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    @TableField(exist = false)
    private Long userId;

    /**
     * 组织名称
     */
    @TableField("organization_name")
    private String organizationName;

    /**
     * 组织地址
     */
    @TableField("organization_address")
    private String organizationAddress;

    /**
     * 组织电话
     */
    @TableField("organization_tel")
    private String organizationTel;

    /**
     * 上级组织
     */
    @TableField("pid")
    private Long pid;

    /**
     * 下级组织数
     */
    @TableField("sub_count")
    private Long subCount;

    /**
     * 排序,数字越小越排前
     */
    @TableField("organization_sort")
    private Long organizationSort;

    /**
     * 组织状态,1启用，0禁用
     */
    @TableField("enabled")
    private Boolean enabled;


    /**
     * 1 组织 2 部门
     */
    @TableField("type")
    private String type;

    @TableLogic
    private Integer delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAddress() {
        return organizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public String getOrganizationTel() {
        return organizationTel;
    }

    public void setOrganizationTel(String organizationTel) {
        this.organizationTel = organizationTel;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getSubCount() {
        return subCount;
    }

    public void setSubCount(Long subCount) {
        this.subCount = subCount;
    }

    public Long getOrganizationSort() {
        return organizationSort;
    }

    public void setOrganizationSort(Long organizationSort) {
        this.organizationSort = organizationSort;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
