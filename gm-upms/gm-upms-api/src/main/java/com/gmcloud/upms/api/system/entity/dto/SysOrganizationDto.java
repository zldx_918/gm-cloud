package com.gmcloud.upms.api.system.entity.dto;

import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;

@Schema(description = "组织入参")
public class SysOrganizationDto {

    @Schema(description = "id")
    @NotNull(message = "角色id不能为空", groups = {BaseEntity.Update.class, BaseEntity.Delete.class})
    private Long id;


    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    private String organizationName;

    /**
     * 组织地址
     */
    @Schema(description = "组织地址")
    private String organizationAddress;

    /**
     * 组织电话
     */
    @Schema(description = "组织电话")
    private String organizationTel;

    /**
     * 上级组织
     */
    @Schema(description = "上级组织id")
    private Long pid;


    /**
     * 排序,数字越小越排前
     */
    @Schema(description = "排序,数字越小越排前")
    private Long organizationSort;

    /**
     * 组织状态,1启用，0禁用
     */
    @Schema(description = "组织状态,1启用，0禁用")
    private Boolean enabled;

    /**
     * 1 组织 2 部门
     */
    @Schema(description = "1 组织,2 部门")
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationAddress() {
        return organizationAddress;
    }

    public void setOrganizationAddress(String organizationAddress) {
        this.organizationAddress = organizationAddress;
    }

    public String getOrganizationTel() {
        return organizationTel;
    }

    public void setOrganizationTel(String organizationTel) {
        this.organizationTel = organizationTel;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getOrganizationSort() {
        return organizationSort;
    }

    public void setOrganizationSort(Long organizationSort) {
        this.organizationSort = organizationSort;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
