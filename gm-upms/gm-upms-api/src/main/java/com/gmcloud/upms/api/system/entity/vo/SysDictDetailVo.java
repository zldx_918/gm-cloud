package com.gmcloud.upms.api.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * @author zl
 * @version 1.0
 * @since 2023-03-16 11:11
 * @description 字典详情vo
 */
@Schema(description = "字典详情vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysDictDetailVo extends BaseEntity {

    /**
     * ID
     */
    @Schema(description = "字典详情id")
    private Long detailId;

    /**
     * 字典id
     */
    @Schema(description = "字典关联id")
    private Long dictId;

    /**
     * 字典标签
     */
    @Schema(description = "字典显示标签")
    private String label;

    /**
     * 字典值
     */
    @Schema(description = "字典详情值")
    private String value;

    /**
     * 排序
     */
    @Schema(description = "字典排序")
    private Integer dictSort;


    /**
     * 是否系统级，无法删除
     */
    private Boolean system;


    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }


    public Long getDictId() {
        return dictId;
    }

    public void setDictId(Long dictId) {
        this.dictId = dictId;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public Integer getDictSort() {
        return dictSort;
    }

    public void setDictSort(Integer dictSort) {
        this.dictSort = dictSort;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }
}