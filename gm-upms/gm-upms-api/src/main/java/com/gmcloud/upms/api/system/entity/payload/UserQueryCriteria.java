package com.gmcloud.upms.api.system.entity.payload;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;
import java.util.Set;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 13:13
 * @description 用户查询条件
 */
@Schema(title = "UserQueryCriteria",description = "用户查询参数")
public class UserQueryCriteria  {

    @Schema(description = "用户id")
    private Long id;

    @Schema(description = "用户多个部门id")
    private Set<Long> deptIds;


    /**
     * email,username,nickName
     */
    @Schema(description = "用户查询email,用户名,别名")
    private String blurry;

    @Schema(description= "用户账号状态")
    private Boolean enabled;


    @Schema(description= "用户ids,用于批量操作")
    private List<Long> ids;

    @Schema(description = "用户排序")
    private List<String> sort;

    public List<String> getSort() {
        return sort;
    }

    public void setSort(List<String> sort) {
        this.sort = sort;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Long> getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(Set<Long> deptIds) {
        this.deptIds = deptIds;
    }

    public String getBlurry() {
        return blurry;
    }

    public void setBlurry(String blurry) {
        this.blurry = blurry;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }


    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
}