package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;

import java.io.Serial;
import java.io.Serializable;

/**
 * 系统菜单
 *
 * @author zl.sir
 * @since 2022-08-26
 */
@TableName("sys_menu")
public class SysMenu extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 723414809556594840L;
    /**
     * ID
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long id;

    /**
     * 上级菜单ID,为空就是布局，0,目录,1,路由,2 按钮
     */
    @TableField("pid")
    private Long pid;

    /**
     * 子菜单数目
     */
    @TableField("sub_count")
    private Long subCount;

    /**
     * 菜单类型
     */
    @TableField("type")
    private Long type;

    /**
     * 菜单标题
     */
    @TableField("title")
    private String title;

    /**
     * 组件名称
     */
    @TableField("name")
    private String name;

    /**
     * 组件
     */
    @TableField("component")
    private String component;

    /**
     * 排序
     */
    @TableField("menu_sort")
    private Long menuSort;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 链接地址
     */
    @TableField("path")
    private String path;

    /**
     * 是否外链
     */
    @TableField("frame")
    private Boolean frame;

    /**
     * 缓存
     */
    @TableField("cache")
    private Boolean cache;

    /**
     * 隐藏
     */
    @TableField("hidden")
    private Boolean hidden;

    /**
     * 权限
     */
    @TableField("permission")
    private String permission;

    /**
     * 是否需要验证
     */
    @TableField("requires_auth")
    private Boolean requiresAuth;


    /**
     * 删除标识（0-正常,1-删除）
     */
    @TableLogic
    private Integer delFlag;

    @TableField(exist = false)
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Long getSubCount() {
        return subCount;
    }

    public void setSubCount(Long subCount) {
        this.subCount = subCount;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Long getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(Long menuSort) {
        this.menuSort = menuSort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public Boolean getFrame() {
        return frame;
    }

    public void setFrame(Boolean frame) {
        this.frame = frame;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }


    public Boolean getRequiresAuth() {
        return requiresAuth;
    }

    public void setRequiresAuth(Boolean requiresAuth) {
        this.requiresAuth = requiresAuth;
    }


    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
