package com.gmcloud.upms.api.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serial;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 13:18
 * @description 用户vo
 */
@Schema(description = "用户vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysUserVo extends BaseEntity {

    @Serial
    private static final long serialVersionUID = -6512189369761197597L;

    @Schema(description = "用户id")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    @Schema(description = "用户名")
    private String username;
    @Schema(description = "昵称")
    private String nickName;

    @Schema(description = "性别")
    private String gender;

    @Schema(description = "联系电话")
    private String phone;

    @Schema(description = "电子邮箱")
    private String email;

    @Schema(description = "头像名称")
    private String avatarName;

    @Schema(description = "头像地址")
    private String avatarPath;

    @Schema(description = "登录密码")
    private String password;

    @Schema(description = "生日")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime birthday;

    @Schema(description = "是否管理员")
    private Boolean admin;

    @Schema(description = "状态：启用、禁用,锁定，密码过期，已注销，更多扩展字典")
    private String status;

    @Schema(description = "密码重置时间")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", shape = JsonFormat.Shape.STRING)
    private LocalDateTime pwdResetTime;


    @Schema(description = "用户角色")
    private List<SysRoleVo> roles;

    @Schema(description = "组织")
    private List<SysOrganizationVo> organizations;

    @Schema(description = "职位")
    private List<SysPostVo> posts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatarName() {
        return avatarName;
    }

    public void setAvatarName(String avatarName) {
        this.avatarName = avatarName;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public List<SysRoleVo> getRoles() {
        return roles;
    }

    public void setRoles(List<SysRoleVo> roles) {
        this.roles = roles;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public List<SysOrganizationVo> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<SysOrganizationVo> organizations) {
        this.organizations = organizations;
    }

    public List<SysPostVo> getPosts() {
        return posts;
    }

    public void setPosts(List<SysPostVo> posts) {
        this.posts = posts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}