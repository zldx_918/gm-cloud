package com.gmcloud.upms.api.system.entity.dto;

import com.gmcloud.upms.api.system.entity.SysPost;
import com.gmcloud.upms.api.system.entity.SysRole;
import com.gmcloud.upms.api.system.entity.SysUser;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 16:29
 */
public class UserInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 6307665296236721620L;
    /**
     * 用户基本信息
     */
    private SysUser sysUser;

    /**
     * 权限标识集合
     */
    private String[] permissions;

    /**
     * 角色集合
     */
    private Long[] roles;

    /**
     * 角色集合
     */
    private List<SysRole> roleList;

    /**
     * 岗位集合
     */
    private Long[] posts;

    /**
     * 岗位集合
     */
    private List<SysPost> postList;

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public String[] getPermissions() {
        return permissions;
    }

    public void setPermissions(String[] permissions) {
        this.permissions = permissions;
    }

    public Long[] getRoles() {
        return roles;
    }

    public void setRoles(Long[] roles) {
        this.roles = roles;
    }

    public List<SysRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRole> roleList) {
        this.roleList = roleList;
    }

    public Long[] getPosts() {
        return posts;
    }

    public void setPosts(Long[] posts) {
        this.posts = posts;
    }

    public List<SysPost> getPostList() {
        return postList;
    }

    public void setPostList(List<SysPost> postList) {
        this.postList = postList;
    }
}
