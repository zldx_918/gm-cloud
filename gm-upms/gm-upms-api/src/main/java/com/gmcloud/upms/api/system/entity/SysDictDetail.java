package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 11:19
 * @description 数据字典详情
 */
@TableName("sys_dict_detail")
public class SysDictDetail extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 800548631091363356L;
    /**
     * ID
     */
    @TableId(value = "dict_detail_id",type = IdType.AUTO)
    private Long id;

    /**
     * 字典id
     */
    @TableField("dict_id")
    private Long dictId;

    /**
     * 字典标签
     */
    @TableField("label")
    private String label;

    /**
     * 字典值
     */
    @TableField("value")
    private String value;

    /**
     * 排序
     */
    @TableField("dict_sort")
    private Integer dictSort;

    /**
     * 系统级别不能删除 0不是 1是
     */
    @TableField("is_system")
    private Boolean system;

    @TableLogic
    private Integer delFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDictId() {
        return dictId;
    }

    public void setDictId(Long dictId) {
        this.dictId = dictId;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public Integer getDictSort() {
        return dictSort;
    }

    public void setDictSort(Integer dictSort) {
        this.dictSort = dictSort;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }
}