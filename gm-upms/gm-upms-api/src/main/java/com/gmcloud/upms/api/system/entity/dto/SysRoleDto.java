package com.gmcloud.upms.api.system.entity.dto;

import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import java.util.List;

@Schema(description = "角色入参")
public class SysRoleDto {

    @Schema(description = "id", required = true)
    @NotNull(message = "角色id不能为空", groups = {BaseEntity.Update.class, BaseEntity.Delete.class})
    private Long id;

    /**
     * 名称
     */
    @Schema(description = "名称", required = true)
    @NotNull(message = "角色id不能为空", groups = {BaseEntity.Insert.class})
    private String roleName;

    /**
     * 角色代码
     */
    @Schema(description = "角色代码", required = true)
    private String roleCode;

    /**
     * 角色级别
     */
    @Schema(description = "角色级别")
    private Integer level;

    /**
     * 描述
     */
    @Schema(description = "描述")
    private String description;

    /**
     * 数据权限
     */
    @Schema(description = "数据权限")
    private String dataScope;

    /**
     * 1激活，0禁用
     */
    @Schema(description = "1激活，0禁用", defaultValue = "1")
    private Boolean enabled;

    @Schema(description = "菜单列表")
    private List<SysMenuDto> menus;

    @Schema(description = "关联菜单ids")
    private List<Long> menuIds;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<SysMenuDto> getMenus() {
        return menus;
    }

    public void setMenus(List<SysMenuDto> menus) {
        this.menus = menus;
    }

    public List<Long> getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(List<Long> menuIds) {
        this.menuIds = menuIds;
    }
}
