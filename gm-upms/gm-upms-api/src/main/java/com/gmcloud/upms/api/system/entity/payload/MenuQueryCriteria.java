package com.gmcloud.upms.api.system.entity.payload;

import com.gmcloud.common.mybatis.annotation.QueryAnnotation;
import com.gmcloud.common.mybatis.base.BaseQueryType;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "菜单查询参数")
public class MenuQueryCriteria {

    /**
     * ID
     */
    @Schema(description = "ID")
    private Long id;

    /**
     * 上级菜单ID,为空就是布局，0,目录,1,路由,2 按钮
     */
    @Schema(description = "上级菜单ID,为空就是布局，0,目录,1,路由,2 按钮")
    @QueryAnnotation(type = BaseQueryType.EQUAL)
    private Long pid;

    /**
     * 菜单类型
     */
    @Schema(description = "菜单类型")
    private Integer type;

    /**
     * 菜单标题
     */
    @Schema(description = "菜单标题")
    private String title;

    /**
     * 是否外链
     */
    @Schema(description = "是否外链")
    private Boolean frame;

    /**
     * 缓存
     */
    @Schema(description = "缓存")
    private Boolean cache;

    /**
     * 隐藏
     */
    @Schema(description = "隐藏")
    private Boolean hidden;

    /**
     * 权限
     */
    @Schema(description = "权限")
    private String permission;

    /**
     * 是否需要验证
     */
    @Schema(description = "是否需要验证")
    private Boolean requiresAuth;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getFrame() {
        return frame;
    }

    public void setFrame(Boolean frame) {
        this.frame = frame;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public Boolean getRequiresAuth() {
        return requiresAuth;
    }

    public void setRequiresAuth(Boolean requiresAuth) {
        this.requiresAuth = requiresAuth;
    }
}
