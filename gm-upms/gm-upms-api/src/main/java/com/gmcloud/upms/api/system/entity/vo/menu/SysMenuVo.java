package com.gmcloud.upms.api.system.entity.vo.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serial;
import java.util.List;

/**
 * @author zl
 * @since  2022/10/20 10:55
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.api.system.vo.menu
 * @description 菜单输出
 */
@Schema(description = "菜单vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysMenuVo extends BaseEntity {

    @Serial
    private static final long serialVersionUID = -5288628153352232251L;
    /**
     * ID
     */
    @Schema(description = "id")
    private Long id;

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    private Long roleId;

    /**
     * 上级菜单ID
     */
    @Schema(description = "上级菜单ID")
    @JsonInclude
    private Long pid;

    /**
     * 子菜单数目
     */
    @Schema(description = "子菜单数目")
    private Long subCount;

    /**
     * 菜单类型
     */
    @Schema(description = "菜单类型")
    private Long type;

    /**
     * 菜单标题
     */
    @Schema(description = "菜单标题")
    private String title;

    /**
     * 组件名称
     */
    @Schema(description = "组件名称")
    private String name;

    /**
     * 组件地址
     */
    @Schema(description = "组件地址")
    private String component;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Long menuSort;

    /**
     * 图标
     */
    @Schema(description = "图标")
    private String icon;

    /**
     * 链接地址
     */
    @Schema(description = "链接地址")
    private String path;

    /**
     * 是否外链
     */
    @Schema(description = "是否外链")
    private Boolean frame;

    /**
     * 缓存
     */
    @Schema(description = "缓存")
    private Boolean cache;

    /**
     * 隐藏
     */
    @Schema(description = "隐藏")
    private Boolean hidden;

    /**
     * 权限
     */
    @Schema(description = "权限")
    private String permission;


    @Schema(description = "子菜单")
    private List<SysMenuVo> children;

    /**
     * 是否需要验证
     */
    @Schema(description = "是否需要验证")
    private Boolean requiresAuth;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getSubCount() {
        return subCount;
    }

    public void setSubCount(Long subCount) {
        this.subCount = subCount;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public void setMenuSort(Long menuSort) {
        this.menuSort = menuSort;
    }

    public Long getMenuSort() {
        return menuSort;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getFrame() {
        return frame;
    }

    public void setFrame(Boolean frame) {
        this.frame = frame;
    }

    public Boolean getCache() {
        return cache;
    }

    public void setCache(Boolean cache) {
        this.cache = cache;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }


    public List<SysMenuVo> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenuVo> children) {
        this.children = children;
    }

    public Boolean getRequiresAuth() {
        return requiresAuth;
    }

    public void setRequiresAuth(Boolean requiresAuth) {
        this.requiresAuth = requiresAuth;
    }


    @Schema(description = "菜单树显示label")
    public String getLabel() {
        return getTitle();
    }


    @Schema(description = "是否有子节点")
    public Boolean getHasChildren() {
        return subCount > 0;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
