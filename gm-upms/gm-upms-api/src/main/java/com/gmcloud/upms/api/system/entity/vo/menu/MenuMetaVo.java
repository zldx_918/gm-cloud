package com.gmcloud.upms.api.system.entity.vo.menu;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zl
 * @since  2022/10/20 10:59
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.api.system.vo.menu
 * @description meta
 */
@Schema(description = "菜单meta")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class MenuMetaVo implements Serializable {

    @Schema(description = "路由标题")
    private String title;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "是否缓存")
    private Boolean noCache;

    /**
     * 请求是否需要验证
     */
    @Schema(description = "是否需要请求验证")
    private Boolean requiresAuth;

    @Schema(description = "是否隐藏")
    private Boolean hidden;

    @Schema(description = "总是显示")
    private Boolean alwaysShow;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getNoCache() {
        return noCache;
    }

    public void setNoCache(Boolean noCache) {
        this.noCache = noCache;
    }

    public Boolean getRequiresAuth() {
        return requiresAuth;
    }

    public void setRequiresAuth(Boolean requiresAuth) {
        this.requiresAuth = requiresAuth;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Boolean getAlwaysShow() {
        return alwaysShow;
    }

    public void setAlwaysShow(Boolean alwaysShow) {
        this.alwaysShow = alwaysShow;
    }
}
