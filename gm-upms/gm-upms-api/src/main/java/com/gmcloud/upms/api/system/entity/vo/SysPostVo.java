package com.gmcloud.upms.api.system.entity.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.gmcloud.common.mybatis.base.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "岗位vo")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SysPostVo extends BaseEntity {

    /**
     * 关联用户id
     */
    @Schema(description = "关联用户id")
    private Long userId;

    @Schema(description = "id")
    private Long id;

    /**
     * 岗位编码
     */
    @Schema(description = "岗位编码")
    private String postCode;

    /**
     * 岗位名称
     */
    @Schema(description = "岗位名称")
    private String postName;

    /**
     * 岗位状态
     */
    @Schema(description = "岗位状态")
    private Boolean enabled;

    /**
     * 排序
     */
    @Schema(description = "排序")
    private Integer postSort;

    /**
     * 备注
     */
    @Schema(description = "备注")
    private Integer remark;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getPostSort() {
        return postSort;
    }

    public void setPostSort(Integer postSort) {
        this.postSort = postSort;
    }

    public Integer getRemark() {
        return remark;
    }

    public void setRemark(Integer remark) {
        this.remark = remark;
    }
}
