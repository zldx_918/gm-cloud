package com.gmcloud.upms.api.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.gmcloud.common.mybatis.base.BaseEntity;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 11:18
 * @description 数据字典
 */
@TableName("sys_dict")
public class SysDict extends BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 8252431218351022738L;
    /**
     * ID
     */
    @TableId(value="dict_id",type = IdType.AUTO)
    private Long id;

    /**
     * 字典名称
     */
    @TableField("dict_name")
    private String dictName;

    /**
     * 描述
     */
    @TableField("description")
    private String description;


    /**
     * 状态
     */
    @TableField("enabled")
    private Boolean enabled;

    /**
     * 系统级别不能删除 0不是 1是
     */
    @TableField("is_system")
    private Boolean system;

    @TableLogic
    private Long delFlag;


    /**
     * 字典详情对应
     */
    @TableField(exist = false)
    private List<SysDictDetail> dictDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Long delFlag) {
        this.delFlag = delFlag;
    }

    public List<SysDictDetail> getDictDetails() {
        return dictDetails;
    }

    public void setDictDetails(List<SysDictDetail> dictDetails) {
        this.dictDetails = dictDetails;
    }

    public Boolean getSystem() {
        return system;
    }

    public void setSystem(Boolean system) {
        this.system = system;
    }
}