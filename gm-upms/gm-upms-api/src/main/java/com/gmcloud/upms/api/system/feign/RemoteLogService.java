package com.gmcloud.upms.api.system.feign;

import com.gmcloud.common.core.constant.SecurityConstants;
import com.gmcloud.common.core.constant.ServiceNameConstants;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysDataLog;
import com.gmcloud.upms.api.system.entity.SysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/18 12:48
 */
@FeignClient(contextId = "remoteLogService", value = ServiceNameConstants.UMPS_SERVICE)
public interface RemoteLogService {
    /**
     * 保存日志
     *
     * @param sysLog 日志实体
     * @param from   内部调用标志
     * @return true、false
     */
    @PostMapping("/log")
    R<Boolean> saveLog(@RequestBody SysLog sysLog, @RequestHeader(SecurityConstants.FROM) String from);

    /**
     * 保存数据审计日志
     *
     * @param sysLog 日志实体
     * @param from   内部调用标志
     * @return true、false
     */
    @PostMapping("/data/log")
    R<Boolean> saveDataLog(SysDataLog sysLog, @RequestHeader(SecurityConstants.FROM) String from);
}
