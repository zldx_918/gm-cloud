package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.upms.api.system.entity.SysDataLog;

public interface SysDataLogService extends IService<SysDataLog> {
}
