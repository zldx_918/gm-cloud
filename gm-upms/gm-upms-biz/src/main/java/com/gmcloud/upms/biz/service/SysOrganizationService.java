package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.upms.api.system.entity.SysOrganization;
import com.gmcloud.upms.api.system.entity.vo.SysOrganizationVo;

import java.util.List;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 17:06
 * @description 单位组织服务
 */
public interface SysOrganizationService extends IService<SysOrganization> {

    /**
     * 查询当前用户的单位组织
     *
     * @param userIds 用户ids
     * @return 用户组织
     */
    Map<Long, List<SysOrganizationVo>> findMapByUserIds(List<Long> userIds);


    /**
     * 获取组织树
     * @return 组织树
     */
    List<SysOrganizationVo> findOrganizationTree();

}
