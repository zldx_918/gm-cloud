package com.gmcloud.upms.biz.controller;

import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.vo.SysOrganizationVo;
import com.gmcloud.upms.biz.service.SysOrganizationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@Tag(name = "管理:组织")
@RestController
@RequestMapping("/organization")
public class OrganizationController {

    private final SysOrganizationService service;

    public OrganizationController(SysOrganizationService service) {
        this.service = service;
    }

    @Operation(summary = "获取组织树")
    @GetMapping("tree")
    public R<List<SysOrganizationVo>> findTree() {
        List<SysOrganizationVo> unitOrganizationVoList = service.findOrganizationTree();
        return R.ok(unitOrganizationVoList);
    }
}
