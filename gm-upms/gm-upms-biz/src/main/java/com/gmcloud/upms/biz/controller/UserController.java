package com.gmcloud.upms.biz.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gmcloud.common.core.exception.ErrorCodes;
import com.gmcloud.common.core.utils.MsgUtil;
import com.gmcloud.common.log.annotation.SysLog;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.common.security.entity.GmUser;
import com.gmcloud.common.security.util.SecurityUtils;
import com.gmcloud.common.utils.R;
import com.gmcloud.common.security.annotation.Inner;
import com.gmcloud.upms.api.system.entity.dto.SysUserDto;
import com.gmcloud.upms.api.system.entity.dto.UserInfo;
import com.gmcloud.upms.api.system.entity.SysUser;
import com.gmcloud.upms.api.system.entity.payload.UserQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.SysUserVo;
import com.gmcloud.upms.biz.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:07
 */
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@Tag(name = "管理:用户")
@RestController
@RequestMapping("/user")
public class UserController {

    private final SysUserService service;

    public UserController(SysUserService service) {
        this.service = service;
    }

    /**
     * 获取指定用户名信息
     *
     * @param username 用户名
     * @return 用户详情
     */
    @Inner
    @Operation(summary = "根据用户名获取指定用户信息")
    @GetMapping("info/{username}")
    public R<UserInfo> info(@PathVariable String username) {
        SysUser sysUser = service.getOne(Wrappers.<SysUser>query().lambda().eq(SysUser::getUsername, username));
        if (Objects.isNull(sysUser)) {
            return R.failed(MsgUtil.getMessage(ErrorCodes.SYS_USER_USERINFO_EMPTY, username));
        }
        return R.ok(service.findUserInfo(sysUser));
    }

    @Operation(summary = "获取当前登录用户详情")
    @GetMapping("/info")
    public R<GmUser> userInfo() {
        return R.ok(SecurityUtils.getUser());
    }


    @Operation(summary = "分页查询用户列表")
    @GetMapping("page")
    public R<BasePageEntity<SysUserVo>> findUserByPage(UserQueryCriteria criteria, Page<SysUser> page) {
        return R.ok(service.page(page, criteria));
    }

    @Operation(summary = "新增用户")
    @SysLog("新增用户")
    @PostMapping
    public R<Void> saveUser(@RequestBody @Validated(BaseEntity.Insert.class) SysUserDto sysUserDto) {
        service.save(sysUserDto);
        return R.ok();
    }

    @Operation(summary = "修改用户")
    @SysLog(value = "修改用户")
    @PutMapping
    public R<Void> updateUser(@RequestBody @Validated(BaseEntity.Update.class) SysUserDto sysUserDto) {
        service.update(sysUserDto);
        return R.ok();
    }

    @Operation(summary = "删除用户")
    @SysLog("删除用户")
    @DeleteMapping
    public R<Void> deleteUser(@RequestBody @Validated({BaseEntity.Delete.class}) SysUserDto sysUserDto) {
        service.delete(sysUserDto);
        return R.ok();
    }


}
