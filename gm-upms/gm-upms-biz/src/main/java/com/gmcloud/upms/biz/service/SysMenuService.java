package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.upms.api.system.entity.SysMenu;
import com.gmcloud.upms.api.system.entity.dto.SysMenuDto;
import com.gmcloud.upms.api.system.entity.payload.MenuQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.menu.NavMenuVo;
import com.gmcloud.upms.api.system.entity.vo.menu.SysMenuVo;

import java.util.List;
import java.util.Set;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 16:50
 */
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 通过角色编号查询URL 权限
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    Set<SysMenu> setByRoleId(Long roleId);


    /**
     * 根据权限列表ids查询菜单路由
     *
     * @param ids id
     * @return 菜单
     */
    List<SysMenuVo> listByRoleIds(Set<Long> ids);


    /**
     * 根据用户id查询菜单
     *
     * @param currentUserId 当前id
     * @return 返回当前用户菜单
     */
    List<SysMenuVo> listByUserId(Long currentUserId);


    /**
     * 构建菜单树
     *
     * @param menuVoList 菜单集合
     * @return 菜单树
     */
    List<SysMenuVo> buildTree(List<SysMenuVo> menuVoList);

    /**
     * 返回当前节点的子节点
     *
     * @param sysMenuVo  当前菜单
     * @param menuVoList 所有菜单集合
     * @return 返回子菜单数据
     */
    List<SysMenuVo> collectTreeData(SysMenuVo sysMenuVo, List<SysMenuVo> menuVoList);

    /**
     * 把菜单树构建显示菜单数据结构
     *
     * @param menuVosTree 菜单树
     * @return 返回显示菜单
     */
    List<NavMenuVo> buildMenus(List<SysMenuVo> menuVosTree);


    /**
     * 分页菜单
     *
     * @param page          分页参数
     * @param queryCriteria 查询条件
     * @return 分页结果
     */
    BasePageEntity<SysMenuVo> page(Page<SysMenu> page, MenuQueryCriteria queryCriteria);

    /**
     * 查询菜单列表
     *
     * @param queryCriteria 查询条件
     * @return 列表
     */
    List<SysMenuVo> list(MenuQueryCriteria queryCriteria);

    /**
     * 查询菜单树
     *
     * @return 树形菜单
     */
    List<SysMenuVo> menuTree();

    /**
     * 添加菜单
     *
     * @param sysMenuDto 菜单
     */
    void save(SysMenuDto sysMenuDto);

    /**
     * 更新菜单
     *
     * @param sysMenuDto 菜单
     */
    void update(SysMenuDto sysMenuDto);

    /**
     * 移除菜单
     *
     * @param sysMenuDto 菜单
     */
    void delete(SysMenuDto sysMenuDto);

    /**
     * 根据角色查询关联菜单
     *
     * @return 菜单
     */
    List<SysMenuVo> findByRoleIds(List<Long> roleIds);
}
