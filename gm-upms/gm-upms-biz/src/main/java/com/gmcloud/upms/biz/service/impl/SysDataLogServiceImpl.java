package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.upms.api.system.entity.SysDataLog;
import com.gmcloud.upms.biz.mapper.SysDataLogMapper;
import com.gmcloud.upms.biz.service.SysDataLogService;
import org.springframework.stereotype.Service;

@Service
public class SysDataLogServiceImpl extends ServiceImpl<SysDataLogMapper, SysDataLog> implements SysDataLogService {
}
