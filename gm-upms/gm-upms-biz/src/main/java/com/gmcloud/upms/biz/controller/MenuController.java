package com.gmcloud.upms.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gmcloud.common.log.annotation.SysLog;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.common.security.util.SecurityUtils;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysMenu;
import com.gmcloud.upms.api.system.entity.dto.SysMenuDto;
import com.gmcloud.upms.api.system.entity.payload.MenuQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.menu.NavMenuVo;
import com.gmcloud.upms.api.system.entity.vo.menu.SysMenuVo;
import com.gmcloud.upms.biz.service.SysMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * @author zl
 * @since  2022/10/20 10:42
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.upms.biz.controller
 * @description menu
 */
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@Tag(name = "管理:菜单")
@RequestMapping("/menu")
@RestController
public class MenuController {

    private final SysMenuService service;

    public MenuController(SysMenuService service) {
        this.service = service;
    }

    @Operation(summary = "导航菜单")
    @GetMapping(value = "/nav/build")
    public R<List<NavMenuVo>> buildNavMenu() {
        List<SysMenuVo> menuVoList = service.listByUserId(Objects.requireNonNull(SecurityUtils.getUser()).getId());
        List<SysMenuVo> menuVosTree = service.buildTree(menuVoList.stream().filter(m -> !Objects.equals(m.getType(), 2L)).toList());
        return R.ok(service.buildMenus(menuVosTree));
    }

    @Operation(summary = "分页查询菜单列表")
    @GetMapping("page")
    public R<BasePageEntity<SysMenuVo>> page(Page<SysMenu> page, MenuQueryCriteria queryCriteria) {
        return R.ok(service.page(page, queryCriteria));
    }

    @Operation(summary = "查询菜单列表")
    @GetMapping
    public R<List<SysMenuVo>> list(MenuQueryCriteria queryCriteria) {
        return R.ok(service.list(queryCriteria));
    }

    @Operation(summary = "查询菜单树")
    @GetMapping("tree")
    public R<List<SysMenuVo>> menuTree() {
        return R.ok(service.menuTree());
    }

    @SysLog("添加菜单")
    @Operation(summary = "添加菜单")
    @PostMapping
    public R<Void> saveMenu(@RequestBody @Validated(BaseEntity.Insert.class) SysMenuDto sysMenuDto) {
        service.save(sysMenuDto);
        return R.ok();
    }

    @SysLog("更新菜单")
    @Operation(summary = "更新菜单")
    @PutMapping
    public R<Void> updateMenu(@RequestBody @Validated(BaseEntity.Update.class) SysMenuDto sysMenuDto) {
        service.update(sysMenuDto);
        return R.ok();
    }

    @SysLog("删除菜单")
    @Operation(summary = "删除菜单")
    @DeleteMapping
    public R<Void> deleteMenu(@RequestBody @Validated(BaseEntity.Delete.class) SysMenuDto sysMenuDto) {
        service.delete(sysMenuDto);
        return R.ok();
    }
}
