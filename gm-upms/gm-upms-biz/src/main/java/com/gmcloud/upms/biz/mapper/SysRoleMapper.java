package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:51
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {


    /**
     * 通过用户ID，查询角色信息
     *
     * @param userId 用户id
     * @return 角色列表
     */
    List<SysRole> findListByUserId(@Param("userId") Long userId);

    /**
     * 查询用户列表关联的对应角色
     *
     * @param userIds 用户ids
     * @return 角色
     */
    List<SysRole> selectListByUserIds(@Param("userIds") List<Long> userIds);

    /**
     * 新增角色与菜单关联
     *
     * @param roleId  角色id
     * @param menuIds 菜单ids
     */
    void insertRoleAndMenus(@Param("roleId") Long roleId, @Param("menuIds") List<Long> menuIds);

    /**
     * 删除角色与菜单关联
     * @param roleId 角色id
     */
    void deleteRoleAndMenus(Long roleId);
}
