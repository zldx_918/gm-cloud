package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.upms.api.system.entity.dto.SysUserDto;
import com.gmcloud.upms.api.system.entity.dto.UserInfo;
import com.gmcloud.upms.api.system.entity.SysUser;
import com.gmcloud.upms.api.system.entity.payload.UserQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.SysUserVo;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:14
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 获取用户详情
     *
     * @param sysUser 用户
     * @return 详情
     */
    UserInfo findUserInfo(SysUser sysUser);

    /**
     * 分页查询
     *
     * @param page     分页
     * @param criteria 查询条件
     * @return 分页结果
     */
    BasePageEntity<SysUserVo> page(IPage<SysUser> page, UserQueryCriteria criteria);

    /**
     * 新增用户
     *
     * @param sysUserDto 接收实体
     */
    void save(SysUserDto sysUserDto);

    /**
     * 更新用户
     *
     * @param sysUserDto 接受实体
     */
    void update(SysUserDto sysUserDto);

    /**
     * 删除用户
     *
     * @param sysUserDto 接收实体
     */
    void delete(SysUserDto sysUserDto);
}
