package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysDataLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDataLogMapper extends BaseMapper<SysDataLog> {
}
