package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.upms.api.system.entity.SysPost;
import com.gmcloud.upms.api.system.entity.vo.SysPostVo;

import java.util.List;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 16:12
 */
public interface SysPostService extends IService<SysPost> {


    /**
     * 根据用户id查询岗位列表
     * @param userId 用户id
     * @return 岗位列表
     */
    List<SysPost> listByUserId(Long userId);

    /**
     * 查询用户关联岗位
     * @param userIds 用户id
     * @return map
     */
    Map<Long, List<SysPostVo>> mapByUserIds(List<Long> userIds);
}
