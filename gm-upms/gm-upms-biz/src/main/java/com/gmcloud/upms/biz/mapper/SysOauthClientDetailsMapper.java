package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysOauthClientDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/25 17:27
 */

@Mapper
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {
}
