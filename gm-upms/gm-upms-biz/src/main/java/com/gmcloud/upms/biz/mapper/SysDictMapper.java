package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 11:26
 * @description 字典mapper
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

    /**
     * 批量查询字典
     * @param dictNames 字典名称集合
     * @return 字典列表
     */
    List<SysDict> findByNames(@Param("dictNames") List<String> dictNames);

    /**
     * 查询字典
     * @param dictName 字典名称
     * @return 字典
     */
    SysDict findByName(@Param("dictName") String dictName);
}
