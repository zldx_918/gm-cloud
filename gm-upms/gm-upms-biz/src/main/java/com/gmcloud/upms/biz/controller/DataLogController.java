package com.gmcloud.upms.biz.controller;

import com.gmcloud.common.security.annotation.Inner;
import com.gmcloud.common.security.entity.GmUser;
import com.gmcloud.common.security.util.SecurityUtils;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysDataLog;
import com.gmcloud.upms.api.system.entity.SysLog;
import com.gmcloud.upms.biz.service.SysDataLogService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@Tag(name = "管理:数据审计日志")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@RestController
@RequestMapping("/data/log")
public class DataLogController {

    private final SysDataLogService service;

    public DataLogController(SysDataLogService service) {
        this.service = service;
    }

    @Inner
    @PostMapping
    public R<Boolean> save(@RequestBody SysDataLog sysLog) {
        return R.ok(service.save(sysLog));
    }

}
