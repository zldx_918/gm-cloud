package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.upms.api.system.entity.SysOauthClientDetails;

/**
 * 客户端服务
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/25 17:28
 */
public interface SysOauthClientDetailsService extends IService<SysOauthClientDetails> {

    /**
     * 通过ID删除客户端
     * @param id 客户端id
     * @return  true/false
     */
    Boolean removeClientDetailsById(String id);

    /**
     * 修改客户端信息
     * @param sysOauthClientDetails  客户端详情
     * @return true/false
     */
    Boolean updateClientDetailsById(SysOauthClientDetails sysOauthClientDetails);

    /**
     * 清除客户端缓存
     */
    void clearClientCache();
}
