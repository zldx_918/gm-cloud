package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.common.utils.BeanUtil;
import com.gmcloud.upms.api.system.entity.SysPost;
import com.gmcloud.upms.api.system.entity.vo.SysPostVo;
import com.gmcloud.upms.biz.mapper.SysPostMapper;
import com.gmcloud.upms.biz.service.SysPostService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 16:30
 * 岗位实现类
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements SysPostService {

    @Override
    public List<SysPost> listByUserId(Long userId) {
        return baseMapper.listPostsByUserId(userId);
    }

    @Override
    public Map<Long, List<SysPostVo>> mapByUserIds(List<Long> userIds) {
        List<SysPost> sysPosts = baseMapper.selectListByUserIds(userIds);
        List<SysPostVo> sysRoleVos= BeanUtil.copyListProperties(sysPosts, SysPostVo::new);
        return sysRoleVos.parallelStream().collect(Collectors.groupingByConcurrent(SysPostVo::getUserId));
    }
}
