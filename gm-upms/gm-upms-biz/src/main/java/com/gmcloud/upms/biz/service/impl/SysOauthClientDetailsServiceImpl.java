package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.common.core.constant.CacheConstants;
import com.gmcloud.upms.api.system.entity.SysOauthClientDetails;
import com.gmcloud.upms.biz.mapper.SysOauthClientDetailsMapper;
import com.gmcloud.upms.biz.service.SysOauthClientDetailsService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/25 17:48
 */
@Service
public class SysOauthClientDetailsServiceImpl extends ServiceImpl<SysOauthClientDetailsMapper, SysOauthClientDetails> implements SysOauthClientDetailsService {


    /**
     * @param id id 通过ID删除客户端
     * @return boolean
     * {@code @CacheEvict} 清除缓存
     * value：缓存位置名称，不能为空，同上
     * key：缓存的key，默认为空，同上
     * condition：触发条件，只有满足条件的情况才会清除缓存，默认为空，支持SpEL
     * allEntries：true表示清除value中的全部缓存，默认为false
     */
    @CacheEvict(value = CacheConstants.CLIENT_DETAILS_KEY, key = "#id")
    @Override
    public Boolean removeClientDetailsById(String id) {
        return this.removeById(id);
    }

    /**
     * 根据客户端信息
     *
     * @param clientDetails 客户端详情
     * @return boolean
     */
    @Override
    @CacheEvict(value = CacheConstants.CLIENT_DETAILS_KEY, key = "#clientDetails.clientId")
    public Boolean updateClientDetailsById(SysOauthClientDetails clientDetails) {
        return this.updateById(clientDetails);
    }

    /**
     * 清除客户端缓存
     */
    @Override
    @CacheEvict(value = CacheConstants.CLIENT_DETAILS_KEY, allEntries = true)
    public void clearClientCache() {
        // TODO document why this method is empty
    }
}
