package com.gmcloud.upms.biz;

import com.gmcloud.common.feign.annotation.EnableGmFeignClients;
import com.gmcloud.common.security.annotation.EnableGmResourceServer;
import com.gmcloud.common.swagger.annotation.EnableGmDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/25 17:09
 * @description 用户统一管理系统
 */
@EnableGmDoc
@EnableGmResourceServer
@EnableGmFeignClients
@SpringBootApplication
public class GmAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmAdminApplication.class, args);
    }
}
