package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 16:12
 * 岗位mapper
 */
@Mapper
public interface SysPostMapper extends BaseMapper<SysPost> {

    /**
     * 根据用户id查询岗位列表
     * @param userId 用户id
     * @return 岗位列表
     */
    List<SysPost> listPostsByUserId(Long userId);

    /**
     * 查询用户岗位列表
     * @param userIds 用户id
     * @return 列表
     */
    List<SysPost> selectListByUserIds(@Param("userIds") List<Long> userIds);
}
