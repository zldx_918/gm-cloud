package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:12
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 用户和角色关联
     *
     * @param userId 用户id
     * @param roles  角色id列表
     */
    void insertUserAndRoles(@Param("userId") Long userId, @Param("roles") List<Long> roles);

    /**
     * 用户和组织关连
     *
     * @param userId        用户id
     * @param organizations 组织id列表
     */
    void insertUserAndOrganizations(@Param("userId") Long userId, @Param("organizations") List<Long> organizations);

    /**
     * 用户和岗位关连
     *
     * @param userId 用户id
     * @param posts  岗位id列表
     */
    void insertUserAndPosts(@Param("userId") Long userId, @Param("posts") List<Long> posts);

    /**
     * 删除用户角色关联
     *
     * @param userId 用户id
     */
    void deleteUserAndRoles(@Param("userId") Long userId);

    /**
     * 删除用户组织关联
     *
     * @param userId 用户id
     */
    void deleteUserAndOrganizations(@Param("userId") Long userId);

    /**
     * 删除用户岗位关联
     *
     * @param userId 用户id
     */
    void deleteUserAndPosts(@Param("userId") Long userId);
}
