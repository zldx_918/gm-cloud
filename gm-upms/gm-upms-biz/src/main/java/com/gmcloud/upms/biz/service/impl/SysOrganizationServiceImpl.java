package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.common.utils.BeanUtil;
import com.gmcloud.common.utils.TreeUtil;
import com.gmcloud.upms.api.system.entity.SysOrganization;
import com.gmcloud.upms.api.system.entity.vo.SysOrganizationVo;
import com.gmcloud.upms.biz.mapper.SysOrganizationMapper;
import com.gmcloud.upms.biz.service.SysOrganizationService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 17:11
 * @description 单位组织服务实现
 */
@Service
public class SysOrganizationServiceImpl extends ServiceImpl<SysOrganizationMapper, SysOrganization> implements SysOrganizationService {


    @Override
    public Map<Long, List<SysOrganizationVo>> findMapByUserIds(List<Long> userIds) {
        List<SysOrganization> sysRoles = baseMapper.selectListByUserIds(userIds);
        List<SysOrganizationVo> sysRoleVos = BeanUtil.copyListProperties(sysRoles, SysOrganizationVo::new);
        return sysRoleVos.parallelStream().collect(Collectors.groupingByConcurrent(SysOrganizationVo::getUserId));
    }

    @Override
    public List<SysOrganizationVo> findOrganizationTree() {
        List<SysOrganization> unitOrganizationList = baseMapper.selectList(null);
        List<SysOrganizationVo> unitOrganizationVos = BeanUtil.copyListProperties(unitOrganizationList, SysOrganizationVo::new);
        return TreeUtil.listToTree(unitOrganizationVos, SysOrganizationVo::getId, SysOrganizationVo::getPid, SysOrganizationVo::getChildren, SysOrganizationVo::setChildren, SysOrganizationVo::getOrganizationSort);
    }


}
