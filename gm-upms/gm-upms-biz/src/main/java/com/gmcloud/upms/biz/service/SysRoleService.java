package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.upms.api.system.entity.SysRole;
import com.gmcloud.upms.api.system.entity.dto.SysRoleDto;
import com.gmcloud.upms.api.system.entity.payload.RoleQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.SysRoleVo;

import java.util.List;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 14:50
 */
public interface SysRoleService extends IService<SysRole> {

    /**
     * 根据用户id获取角色
     *
     * @param userId userId
     * @return 角色列表
     */
    List<SysRole> findListByUserId(Long userId);

    /**
     * 获取用户对应角色map
     *
     * @param userIds 角色ids
     * @return map
     */
    Map<Long, List<SysRoleVo>> findMapByUserIds(List<Long> userIds);


    /**
     * 查询所有的角色名称
     *
     * @return 角色
     */
    List<SysRoleVo> getRoles();

    /**
     * 分页查询角色
     *
     * @param iPage         分页
     * @param queryCriteria 查询条件
     * @return 角色列表
     */
    BasePageEntity<SysRoleVo> page(Page<SysRole> iPage, RoleQueryCriteria queryCriteria);

    /**
     * 保存角色
     *
     * @param sysRoleDto 角色dto
     */
    void saveRole(SysRoleDto sysRoleDto);

    /**
     * 更新角色
     *
     * @param sysRoleDto 角色dto
     */
    void updateRole(SysRoleDto sysRoleDto);

    /**
     * 删除角色
     *
     * @param sysRoleDto 角色dto
     */
    void removeRole(SysRoleDto sysRoleDto);

    /**
     * 新增角色和菜单关联
     *
     * @param roleId  角色id
     * @param menuIds 菜单id集合
     */
    void insertRoleAndMenus(Long roleId, List<Long> menuIds);

    /**
     * 删除角色和菜单关联
     * @param roleId 角色id
     */
    void deleteRoleAndMenus(Long roleId);
}
