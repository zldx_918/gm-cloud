package com.gmcloud.upms.biz.controller;

import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.vo.SysDictVo;
import com.gmcloud.upms.biz.service.SysDictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@Tag(name = "管理:字典")
@RestController
@RequestMapping("/dict")
public class DictController {


    private final SysDictService dictService;

    public DictController(SysDictService dictService) {
        this.dictService = dictService;
    }

    @Operation(summary = "根据字典名称集合批量获取字典", method = "GET")
    @Parameter(name = "dictNames", description = "字典名称集合",required = true,in = ParameterIn.QUERY)
    @GetMapping("dictNames")
    public R<List<SysDictVo>> findDictByNames(@RequestParam List<String> dictNames) {
        return R.ok(dictService.findByNames(dictNames));
    }
}
