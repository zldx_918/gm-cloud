package com.gmcloud.upms.biz.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gmcloud.common.log.annotation.SysLog;
import com.gmcloud.common.mybatis.base.BaseEntity;
import com.gmcloud.common.mybatis.base.BasePageEntity;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysRole;
import com.gmcloud.upms.api.system.entity.dto.SysRoleDto;
import com.gmcloud.upms.api.system.entity.payload.RoleQueryCriteria;
import com.gmcloud.upms.api.system.entity.vo.SysRoleVo;
import com.gmcloud.upms.biz.service.SysRoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@RestController
@Tag(name = "管理:角色")
@RequestMapping("/role")
public class RoleController {
    private final SysRoleService service;

    public RoleController(SysRoleService service) {
        this.service = service;
    }

    @GetMapping
    @Operation(summary = "查询所有角色")
    public R<List<SysRoleVo>> getRoles() {
        return R.ok(service.getRoles());
    }

    @Operation(description = "分页查询角色列表")
    @GetMapping("page")
    public R<BasePageEntity<SysRoleVo>> page(Page<SysRole> iPage, RoleQueryCriteria queryCriteria) {
        return R.ok(service.page(iPage, queryCriteria));
    }

    @SysLog("添加角色")
    @Operation(summary = "添加角色")
    @PostMapping
    public R<Void> saveRole(@RequestBody @Validated(BaseEntity.Insert.class) SysRoleDto sysRoleDto) {
        service.saveRole(sysRoleDto);
        return R.ok();
    }

    @SysLog("更新角色")
    @Operation(summary = "更新角色")
    @PutMapping
    public R<Void> updateRole(@RequestBody @Validated(BaseEntity.Update.class) SysRoleDto sysRoleDto) {
        service.updateRole(sysRoleDto);
        return R.ok();
    }

    @SysLog("删除角色")
    @Operation(summary = "删除角色")
    @DeleteMapping
    public R<Void> deleteRole(@RequestBody @Validated(BaseEntity.Delete.class) SysRoleDto sysRoleDto) {
        service.removeRole(sysRoleDto);
        return R.ok();
    }

}
