package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysOrganization;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-27 17:13
 * @description 单位组织mapper
 */
@Mapper
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

    /**
     * 查询用户组织列表
     * @param userIds 用户id
     * @return 列表
     */
    List<SysOrganization> selectListByUserIds(@Param("userIds") List<Long> userIds);
}
