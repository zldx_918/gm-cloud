package com.gmcloud.upms.biz.controller;

import com.gmcloud.common.security.annotation.Inner;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysLog;
import com.gmcloud.upms.biz.service.SysLogService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "管理:日志")
@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@RestController
@RequestMapping("/log")
public class LogController {

    private final SysLogService service;

    public LogController(SysLogService service) {
        this.service = service;
    }

    @Inner
    @PostMapping
    public R<Boolean> save(@RequestBody SysLog sysLog) {
        return R.ok(service.save(sysLog));
    }
}
