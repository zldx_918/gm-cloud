package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.upms.api.system.entity.SysLog;
import com.gmcloud.upms.biz.mapper.SysLogMapper;
import com.gmcloud.upms.biz.service.SysLogService;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {
}
