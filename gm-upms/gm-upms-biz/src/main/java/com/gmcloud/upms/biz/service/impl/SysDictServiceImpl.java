package com.gmcloud.upms.biz.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gmcloud.common.utils.BeanUtil;
import com.gmcloud.upms.api.system.entity.SysDict;
import com.gmcloud.upms.api.system.entity.SysDictDetail;
import com.gmcloud.upms.api.system.entity.vo.SysDictDetailVo;
import com.gmcloud.upms.api.system.entity.vo.SysDictVo;
import com.gmcloud.upms.biz.mapper.SysDictMapper;
import com.gmcloud.upms.biz.service.SysDictService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 11:15
 * @description 字典serviceImpl
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {


    @Override
    public List<SysDictVo> findByNames(List<String> dictNames) {
        List<SysDict> sysDictList = baseMapper.findByNames(dictNames);
        return BeanUtil.copyListProperties(sysDictList, SysDictVo::new, (s, t) -> t.setDictDetails(BeanUtil.copyListProperties(s.getDictDetails(), SysDictDetailVo::new))
        );
    }

    @Override
    public Map<String, String> findByName(String dictName) {
        SysDict dict = baseMapper.findByName(dictName);
        if (Objects.nonNull(dict)) {
            List<SysDictDetail> sysDictDetail = dict.getDictDetails();
            return sysDictDetail.stream().collect(Collectors.toConcurrentMap(SysDictDetail::getValue, SysDictDetail::getLabel));
        }
        return new HashMap<>();
    }
}
