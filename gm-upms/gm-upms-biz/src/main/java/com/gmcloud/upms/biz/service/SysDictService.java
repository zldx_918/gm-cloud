package com.gmcloud.upms.biz.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gmcloud.upms.api.system.entity.SysDict;
import com.gmcloud.upms.api.system.entity.vo.SysDictVo;

import java.util.List;
import java.util.Map;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2023-03-16 11:02
 * @description 字典service
 */
public interface SysDictService  extends IService<SysDict>{

    /**
     * 根据字典名称批处理获取字典详情
     * @param dictNames 字典名称集合
     * @return 字典详情
     */
    List<SysDictVo> findByNames(List<String> dictNames);


    /**
     * 根据字典名称查询详情
     * @param dictName 字典名称
     * @return 字典详情
     */
    Map<String,String> findByName(String dictName);

}
