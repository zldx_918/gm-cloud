package com.gmcloud.upms.biz.controller;

import com.gmcloud.common.utils.BeanUtil;
import com.gmcloud.common.utils.R;
import com.gmcloud.upms.api.system.entity.SysPost;
import com.gmcloud.upms.api.system.entity.vo.SysPostVo;
import com.gmcloud.upms.biz.service.SysPostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SecurityRequirement(name = HttpHeaders.AUTHORIZATION)
@Tag(name = "管理:岗位")
@RestController
@RequestMapping("post")
public class PostController {

    private final SysPostService service;

    public PostController(SysPostService service) {
        this.service = service;
    }

    /**
     * 查询岗位列表
     *
     * @return 岗位
     */
    @GetMapping
    @Operation(summary = "查询职位")
    public R<List<SysPostVo>> list() {
        List<SysPost> sysPosts = service.list();
        return R.ok(BeanUtil.copyListProperties(sysPosts, SysPostVo::new));
    }

}
