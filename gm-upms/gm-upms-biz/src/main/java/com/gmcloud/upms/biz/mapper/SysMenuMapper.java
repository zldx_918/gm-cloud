package com.gmcloud.upms.biz.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gmcloud.upms.api.system.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/26 16:37
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {


    /**
     * 通过角色编号查询URL 权限
     * @param roleId 角色ID
     * @return 菜单列表
     */
    Set<SysMenu> listMenusByRoleId(Long roleId);

    /**
     * 根据角色id列表查询
     * @param ids  角色ids
     * @return  菜单
     */
    List<SysMenu> findListByRoleIds(@Param("ids") Set<Long> ids);

    /**
     * 根据角色id查询关联菜单
     * @param roleIds 角色ids
     * @return 菜单列表
     */
    List<SysMenu> selectListByRoleIds(@Param("roleIds") List<Long> roleIds);
}
