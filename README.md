

## 系统说明

- 基于 Spring Cloud 2022 、Spring Boot 2.7.9、 OAuth2 的 RBAC **权限管理系统**
- 不涉及商业行为，如果存在侵权问题，请联系我！！！



## 快速开始

### 核心依赖

| 依赖                   | 版本         |
| ---------------------- |------------|
| Spring Boot            | 2.7.9      |
| Spring Cloud           | 2021.0.1   |
| Spring Cloud Alibaba   | 2021.0.1.0 |
| Spring Security OAuth2 | 2.3.6      |
| Mybatis Plus           | 3.5.1      |
| hutool                 | 5.7.21     |
### 模块说明

```lua

gm
├── gm-auth -- 授权服务提供[3000]
└── gm-common -- 系统公共模块
     ├── gm-common-bom -- 全局依赖管理控制
     ├── gm-common-core -- 公共工具类核心包
     ├── gm-common-datasource -- 动态数据源包
     ├── gm-common-job -- xxl-job 封装
     ├── gm-common-log -- 日志服务
     ├── gm-common-mybatis -- mybatis 扩展封装
     ├── gm-common-security -- 安全工具类
     ├── gm-common-swagger -- 接口文档
     ├── gm-common-feign -- feign 扩展封装
     └── gm-common-test -- oauth2.0 单元测试扩展封装  ，没写
      -- 其他新增模块详情代码 
├── gm-register -- Nacos Server[8848]
├── gm-gateway -- Spring Cloud Gateway网关[9999]
└── gm-upms -- 通用用户权限管理模块
     └── gm-upms-api -- 通用用户权限管理系统公共api模块
     └── gm-upms-biz -- 通用用户权限管理系统业务处理模块[4000]
└── gm-visual
     └── gm-monitor -- 服务监控 [5001]
     ├── gm-codegen -- 图形化代码生成 [5002]   未使用，与实际的前端框架不符，需要改造
     ├── gm-sentinel-dashboard -- 流量高可用 [5003]
     └── gm-xxl-job-admin -- 分布式定时任务管理台 [5004]
```

### 本地开发 运行
http://gm-gateway:9999/swagger-ui.html

## 开源共建

### 开源协议

### 其他说明
学习笔记博客地址:[我的博客](http://www.zhanghuanfu.top/)

### **centos7配置本地host**

`修改主机名在/ect/hostname 和/ect/hosts
这两个文件控制 首先修改/etc/hostname vim /etc/hostname 
打开之后的内容是: localhost.localdomain 把它修改成你想要的名字就可以,比如: niukou.com 保存退出 
然后修改/etc/hosts文件 vim /etc/hosts 打开之后的内容是: 127.0.0.1   localhost localhost.localdomain local****__****
`
