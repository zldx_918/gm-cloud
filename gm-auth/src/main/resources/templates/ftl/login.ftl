<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gm 微服务统一认证</title>

    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/5.2.1/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/signin.css"/>
</head>

<body class="sign_body">
<div class="container form-margin-top">
    <form class="form-signin" action="/token/form" method="post">
        <h2 class="form-signin-heading text-center" >统一认证系统</h2>
        <input type="hidden" name="client_id" class="form-control" value="gm" placeholder="所属客户端">
        <input type="hidden" name="grant_type" class="form-control" value="password" placeholder="所属客户端">
        <input type="text" name="username" class="form-control form-margin-top" placeholder="账号" required autofocus>
        <input type="password" name="password" class="form-control" placeholder="密码" required>
        <button class="btn btn-lg btn-primary btn-block w-100" type="submit">sign in</button>
        <#if error??>
            <span style="color: red; ">${error}</span>
        </#if>
    </form>
</div>
<footer>
    <p>support by: gmcloud</p>
    <p>email: <a href="xxxx@qq.com">xxxxx@qq.com</a>.</p>
</footer>
</body>
</html>
