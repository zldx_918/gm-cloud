<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no"/>
    <title>Gm 第三方授权</title>
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/5.2.1/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="/css/signin.css"/>

</head>

<body>
<div class="container">
    <header class="d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32">
                <use xlink:href="#bootstrap"></use>
            </svg>
            <span class="fs-4">统一认证授权中心</span>
        </a>

        <ul class="nav nav-pills">
            <li class="nav-item"><a href="#" class="nav-link active" aria-current="page">Home</a></li>
            <li class="nav-item"><a href="#" class="nav-link">开放平台</a></li>
            <li class="nav-item">
                <p class="navbar-text navbar-right">
                    <a target="_blank" href="https://gmcloud.com">技术支持</a>
                </p>
            </li>
            <li class="nav-item">
                <p class="navbar-text navbar-right">
                    <#if principalName=="anonymousUser">
                        未登录
                    <#else>
                        <a target="_blank" href="https://gmcloud.com">${principalName}</a>
                    </#if>
                </p>
            </li>
        </ul>
    </header>
    <div style="padding-top: 80px;width: 400px; color: #555; margin:0px auto;">
        <form id='confirmationForm' name='confirmationForm' action="/oauth2/authorize" method='post'>

            <div class="form-floating d-none">
                <input type="hidden" class="form-control" name="client_id" id="id" value="${id}">
                <label for="id" class="col-sm-2 col-form-label">id</label>
            </div>

            <div class="form-floating d-none">
                <input type="hidden" class="form-control" id="state" name="state" value="${state}">
                <label for="state" class="col-sm-2 col-form-label">state</label>
            </div>


            <div class="mb-3 row">
                <p>
                    将获得以下权限：</p>
                <ul class="list-group">
                    <li class="list-group-item"> <span>
              <#list scopeList as scope>
                  <input type="checkbox" checked name="scope" value="${scope}"/><label>scope-${scope}</label>
              </#list>
                </ul>
                <p class="help-block">授权后表明你已同意 <a>服务协议</a></p>
                <button class="btn btn-success pull-right" type="submit" id="write-email-btn">授权</button>
                </p>
            </div>
        </form>
    </div>
</div>
<footer>
    <p>support by: gmcloud.com</p>
    <p>email: <a href="xxxxx@gmail.com">xxxxxx@gmail.com</a>.</p>
</footer>
</body>
</html>
