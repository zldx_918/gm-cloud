package com.gmcloud.auth.config;

import com.gmcloud.auth.support.core.FormIdentityLoginConfigurer;
import com.gmcloud.auth.support.core.GmDaoAuthenticationProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;


/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 22:18
 * 服务安全相关配置
 */
@EnableWebSecurity(debug = true)
public class WebSecurityConfiguration {
    /**
     * spring security 默认的安全策略
     *
     * @param http security注入点
     * @return SecurityFilterChain
     * @throws Exception
     */
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        // 开放自定义的部分端点
        http.authorizeRequests(authorizeRequests -> authorizeRequests.antMatchers("/token/*").permitAll()
                        // 避免iframe同源无法登录
                        .anyRequest().authenticated()).headers().frameOptions().sameOrigin()
                // 表单登录个性化
                .and().apply(new FormIdentityLoginConfigurer());

        // 处理 UsernamePasswordAuthenticationToken   验证登录用户密码
        http.authenticationProvider(new GmDaoAuthenticationProvider());
        return http.build();
    }


    /**
     * 暴露静态资源
     * <p>
     * https://github.com/spring-projects/spring-security/issues/10938
     *
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    @Order(0)
    SecurityFilterChain resources(HttpSecurity http) throws Exception {
        http.requestMatchers(matchers -> matchers.antMatchers("/actuator/**", "/css/**", "/error","/webjars/**"))
                .authorizeHttpRequests(authorize -> authorize.anyRequest().permitAll()).requestCache().disable()
                .securityContext().disable().sessionManagement().disable();
        return http.build();
    }

}
