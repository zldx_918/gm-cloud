package com.gmcloud.auth;

import com.gmcloud.common.feign.annotation.EnableGmFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/8/17 19:22
 */
@EnableGmFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class GmAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(GmAuthApplication.class, args);
    }

}
