package com.gmcloud.test.jasypt.common;

/**
 * @author zl
 */
public interface Const {


    /**
     * 加密
     */
      String encrypt="encrypt";

    /**
     * 解密
     */
     String decrypt="decrypt";

}
