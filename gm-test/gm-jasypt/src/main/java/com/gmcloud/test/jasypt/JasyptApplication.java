package com.gmcloud.test.jasypt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/9/6 19:38
 */
@SpringBootApplication
public class JasyptApplication {

    public static void main(String[] args) {
        SpringApplication.run(JasyptApplication.class,args);
    }
}
