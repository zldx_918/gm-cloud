package com.gmcloud.test.jasypt.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author zl.sir
 * @version 1.0
 * @since 2022/9/7 1:01
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JasyptDto {


    /**
     * 标签切换
     */
    private String tab;

    /**
     * 根密钥
     */
    private String key;

    /**
     * 加解密原文
     */
    private String original;

    /**
     * encrypt：加密   /  decrypt: 解密
     */
    private String type;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }
}
