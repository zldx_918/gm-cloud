<#assign base=request.contextPath />
<!DOCTYPE html>
<html lang="en">
<head>
    <base id="base" href="${base}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gm 加密服务</title>

    <link href="webjars/bootstrap/5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="webjars/jquery/3.6.1/dist/jquery.min.js"></script>
    <script src="webjars/bootstrap/5.2.1/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a id="config" class="nav-link active" aria-current="page" href="#">1.配置文件加解密</a>
                    </li>
                    <li class="nav-item">
                        <a id="basic" class="nav-link" aria-current="page" href="#">2.请求头Basic加解密</a>
                    </li>
                    <li class="nav-item">
                        <a id="password" class="nav-link" aria-current="page" href="#">3.前端密码传输加解密</a>
                    </li>
                </ul>
                <form class="d-flex" role="search">
                    <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success" type="submit">Search</button>
                </form>
            </div>
        </div>
    </nav>
    <div class="row align-items-start">
        <div class="col-6 d-inline-flex flex-column">
            <form class="row mt-3">
                <div class="mb-3 row">
                    <label for="operation" class="col-sm-2 col-form-label">操作方式</label>
                    <div class="col-sm-6">
                        <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                            <input type="radio" class="btn-check" name="mode" id="encrypt" autocomplete="off" checked
                                   value="encrypt">
                            <label class="btn btn-outline-primary" for="encrypt">加密</label>

                            <input type="radio" class="btn-check" name="mode" id="decrypt" autocomplete="off"
                                   value="decrypt">
                            <label class="btn btn-outline-primary" for="decrypt">解密</label>

                        </div>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label id="keyLabel" for="key" class="col-sm-2 col-form-label">根密钥</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="key" value="" aria-describedby="validationServerKeyFeedback" required maxlength="16" minlength="16">
                        <div id="validationServerKeyFeedback" class="invalid-feedback">
                            密码加密根密钥长度必须为16
                        </div>

                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="original" class="col-sm-2 col-form-label" id="originalLabel">加密原文</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="original">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="result" class="col-sm-2 col-form-label">结果</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="result">
                    </div>
                </div>
                <div class="mb-3 row p-5 d-inline-flex">
                    <button type="submit" class="btn btn-primary m-auto w-25" id="submitForm">确认</button>
                </div>
            </form>
        </div>
        <div class="col-6 d-inline-flex flex-column">
            <div id="config-description"><span>配置文件加解密 针对的业务场景为 配置文件中的明文使用 ENC(密文) 的形式替代</span> <span>
                    <img src="${base}/static/images/1660281415.jpg" class="img img-fluid"></span>
                <div class="el-divider el-divider--horizontal"><!----></div>
                <span>配置文件加解密 根密钥默认为 Nacos/application-dev.yml/ gm</span> <span><img
                            src="${base}/static/images/20220907155011.png" class="img"></span>
            </div>

            <div id="basic-description" style="display: none">
                <div>
                    <span>请求头Basic 针对的业务场景为 登录过程中 Header 参数</span>
                    <span>
                        <img src="${base}/static/images/1660281463.jpg" class="img img-fluid">
                    </span>
                    <div class="dropdown-divider"><!----></div>
                    <span>client 信息对应 sys_oauth_client_details 表</span>
                    <span>
                        <img src="${base}/static/images/1660281475.jpg" class="img img-fluid">
                    </span>
                </div>
            </div>
            <div id="password-description" style="display: none">
                <div>
                    <span>前端密码传输加解密 针对的业务场景为 登录过程中 密码参数加密</span>
                    <span>
                        <img src="${base}/static/images/1660281490.jpg" class="img img-fluid">
                    </span>
                    <div class="dropdown-divider"><!----></div>
                    <span>密钥对应 UI/src/store/module/user.js 必须为16位</span>
                    <span>
                        <img src="${base}/static/images/1660281505.jpg" class="img img-fluid">
                    </span>
                </div>
            </div>

        </div>
    </div>
</div>

<script src="${base}/static/js/jasypt.js"></script>

</body>
</html>