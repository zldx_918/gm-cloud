$(document).ready(function () {

    let tab = "config"

    /**
     * 方式切换
     */
    $('.navbar-nav .nav-link').click(function () {
        $('.navbar-nav .nav-link.active').removeClass("active")
        $(this).addClass("active")
        $('#' + tab + '-description').hide()
        const id = $(this).attr("id")
        $('#' + id + '-description').show()
        tab = id

        $("#key").removeClass('is-valid')
        switch (id) {
            case 'config':
                $('#keyLabel').html("根密钥")
                $('#originalLabel').html("加密原文")
                break
            case 'basic':
                $('#keyLabel').html("client_id")
                $('#originalLabel').html("client_secret")
                break
            case 'password':
                $('#keyLabel').html("根密钥")
                $('#originalLabel').html("加密原文")
                // $("#key").addClass('is-valid')
                if ($("#key").val().length === 16) {
                    $("#key").addClass('is-valid')
                }
                break
            default:
                console.log("不支持此操作")
        }

        cleanData()
    })

    /**
     * 加密、解密切换
     */

    $('.btn-group>.btn').click(function () {
        const type = $('.btn-check:active, .btn-check:checked').val()
        if (type !== 'encrypt') {
            $('#originalLabel').html("加密原文")
        } else if (type !== 'decrypt') {
            $('#originalLabel').html("解密原文")
        }
        cleanData()
    })

    /**
     * 提交
     */
    $("button#submitForm").click(function () {
        const key = $("#key").val();
        const original = $("#original").val();
        const type = $('.btn-check:active, .btn-check:checked').val()


        if (tab === 'password') {
            if (key.length === 16) {
                $("#key").addClass('is-valid')
            } else {
                return
            }
        }

        $.ajax({
            url: "/jasypt/encrypted",
            type: 'post',
            contentType: 'application/json',
            data: JSON.stringify({
                key, original, type, tab
            }), success: function (data, status) {
                if (data.code === 200) {
                    $("#result").val(data.result)
                    alert('success')
                } else {
                    alert(data.message)
                }

            }
        })
    })

    /**
     * 清理数据
     */
    function cleanData() {
        $("#original").val('');
        $("#result").val('');
    }

})
