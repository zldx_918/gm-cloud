package com.gmcloud.gmexperiment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GmExperimentApplication {

    public static void main(String[] args) {
        SpringApplication.run(GmExperimentApplication.class, args);
    }

}
