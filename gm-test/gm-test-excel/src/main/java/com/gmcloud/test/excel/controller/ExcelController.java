package com.gmcloud.test.excel.controller;

import com.gmcloud.common.utils.JsonUtil;
import com.gmcloud.common.excel.annotation.RequestExcel;
import com.gmcloud.common.excel.annotation.ResponseExcel;
import com.gmcloud.common.excel.annotation.Sheet;
import com.gmcloud.common.excel.vo.ErrorMessage;
import com.gmcloud.test.excel.dto.StudentDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * @author zl.sir
 * @since  2022/9/21 13:36
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.test.excel.controller
 * @description 操作
 */
@RestController
@RequestMapping("excel")
public class ExcelController {

    private static final Logger logger = LoggerFactory.getLogger(ExcelController.class);
    @PostMapping("/upload")
    public void upload(@RequestExcel List<StudentDto> dataList, BindingResult bindingResult) {
        // JSR 303 校验通用校验获取失败的数据
        List<ErrorMessage> errorMessageList = (List<ErrorMessage>) bindingResult.getTarget();
        logger.info("上传excel数据:{}" , JsonUtil.objToString(dataList));

       logger.info(JsonUtil.objToString(errorMessageList));

    }

    @ResponseExcel(name = "test", password = "123456", sheets = @Sheet(sheetName = "testSheet1"))
    @GetMapping("/get")
    public List<StudentDto> e1() {
        List<StudentDto> dataList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            data.setOther("小机灵鬼");
            dataList.add(data);
        }
        return dataList;
    }

    @ResponseExcel(name = "test", sheets = {
            @Sheet(sheetName = "第一个Sheet"),
            @Sheet(sheetName = "第二个sheet")
    })
    @GetMapping("/gets")
    public List<List<StudentDto>> gets() {
        List<List<StudentDto>> lists = new ArrayList<>();

        List<StudentDto> dataList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            data.setOther("小机灵鬼");
            dataList.add(data);
        }
        lists.add(dataList);

        List<StudentDto> dataList2 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            data.setOther("小机灵鬼");
            dataList2.add(data);
        }
        lists.add(dataList2);
        return lists;
    }


    @ResponseExcel(name = "不同的sheet导出", sheets = {
            @Sheet(sheetName = "第一个Sheet", includes = {"name"}),
            @Sheet(sheetName = "第二个sheet", excludes = {"classz"})
    })
    @GetMapping("/getsOther")
    public List<List<StudentDto>> eOther() {
        List<List<StudentDto>> lists = new ArrayList<>();

        List<StudentDto> dataList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            data.setOther("小机灵鬼");
            dataList.add(data);
        }
        lists.add(dataList);

        List<StudentDto> dataList2 = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            data.setOther("小机灵鬼");
            dataList2.add(data);
        }
        lists.add(dataList2);
        return lists;
    }


    @ResponseExcel(name = "模板测试excel", template = "example.xlsx")
    @GetMapping("/template")
    public List<StudentDto> template() {
        List<StudentDto> dataList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            StudentDto data = new StudentDto();
            data.setId(1L);
            data.setName("张三");
            data.setPassword("123456");
            data.setClassz("舞蹈班");
            dataList.add(data);
        }
        return dataList;
    }
}
