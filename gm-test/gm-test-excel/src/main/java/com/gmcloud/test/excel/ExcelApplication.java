package com.gmcloud.test.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;

/**
 * @author zl.sir
 * @since  2022/9/21 13:35
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.test.excel
 * @description 启动类
 */
@SpringBootApplication(exclude = {RedisAutoConfiguration.class})
public class ExcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelApplication.class,args);
    }

}
