package com.gmcloud.test.excel.dto;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentStyle;
import com.alibaba.excel.enums.poi.FillPatternTypeEnum;

import javax.validation.constraints.NotNull;

/**
 * @author zl.sir
 * @since  2022/9/21 13:42
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.test.excel.controller.dto
 * @description 学生dto
 */
public class StudentDto {


    @NotNull(message = "id不能为空")
    @ExcelProperty(index = 0)
    private Long id;


    /**
     * {@code @ColumnWidth} 定义宽度
     * {@code @ExcelProperty} 定义列名称
     */
    @ColumnWidth(50)
    @ExcelProperty("用户名")
    @ContentStyle(fillPatternType = FillPatternTypeEnum.SOLID_FOREGROUND, fillForegroundColor = 40)
    private String name;

    @ExcelProperty(index = 2)
    private String password;

    @ExcelProperty(index = 3)
    private String classz;


    /**
     *  {@code @ExcelIgnore} 导出忽略这个字段
     */
    @ExcelIgnore
    private String other;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClassz() {
        return classz;
    }

    public void setClassz(String classz) {
        this.classz = classz;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }
}
