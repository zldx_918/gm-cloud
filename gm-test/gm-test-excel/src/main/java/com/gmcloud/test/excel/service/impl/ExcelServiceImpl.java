package com.gmcloud.test.excel.service.impl;

import com.gmcloud.common.utils.JsonUtil;
import com.gmcloud.common.excel.service.ExcelService;
import com.gmcloud.test.excel.dto.StudentDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zl.sir
 * @since  2022/9/21 17:19
 * @BelongsProject gm-cloud
 * @BelongsPackage com.gmcloud.test.excel.service.impl
 * @description service impl
 */
@Service
public class ExcelServiceImpl implements ExcelService<StudentDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelServiceImpl.class);

    @Override
    public void batchInsertExcel(List<StudentDto> dtos) {
        LOGGER.info("保存excel数据 {} ", JsonUtil.objToString(dtos));
    }


}
